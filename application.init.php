<?php
/**
* @package   me
* @subpackage
* @author    Ulukyn
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/
$vendorDir = realpath(__DIR__.'/../vendor/').'/';
require($vendorDir.'autoload.php');

jApp::initPaths(
    __DIR__.'/',
    __DIR__.'/www/',
    __DIR__.'/var/',
    __DIR__.'/var/log/',
    __DIR__.'/var/config/'
);
jApp::setTempBasePath(__DIR__.'/temp/');

require($vendorDir.'jelix_app_path.php');

// Declares here paths of directories containings plugins and modules,
// that are not already declared into composer.json files of Composer packages
jApp::declareModulesDir(array(__DIR__.'/modules/'));
jApp::declarePluginsDir(array(__DIR__.'/plugins'));

function ry_autoload($class)
{
	if (__NAMESPACE__ == 'Ry') {
		$f = __DIR__.'/modules/api/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (__NAMESPACE__ == 'Support') {
		$f = __DIR__.'/modules/api/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (substr($class, 0, 3) == 'Ry\\' && substr($class, 0, 4) !== 'Ry\\j') {
		$f = __DIR__.'/modules/api/classes/'.substr($class, 3).'.php';
		include_once($f);
	} elseif (__NAMESPACE__ == 'Tr') {
		$f = __DIR__.'/modules/translator/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (substr($class, 0, 3) == 'Tr\\' && substr($class, 0, 4) !== 'Tr\\j') {
		$f = __DIR__.'/modules/translator/classes/'.substr($class, 3).'.php';
		include_once($f);
	} elseif (__NAMESPACE__ == 'Billing') {
		$f = __DIR__.'/modules/billing/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (substr($class, 0, 8) == 'Billing\\' && substr($class, 0, 9) !== 'Billing\\j') {
		$f = __DIR__.'/modules/billing/classes/'.substr($class, 8).'.php';
		include_once($f);
	} elseif (__NAMESPACE__ == 'Support') {
		$f = __DIR__.'/modules/support/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (substr($class, 0, 8) == 'Support\\' && substr($class, 0, 9) !== 'Support\\j') {
		$f = __DIR__.'/modules/support/classes/'.substr($class, 8).'.php';
		include_once($f);
	} elseif (__NAMESPACE__ == 'Marketing') {
		$f = __DIR__.'/modules/marketing/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (substr($class, 0, 10) == 'Marketing\\' && substr($class, 0, 11) !== 'Marketing\\j') {
		$f = __DIR__.'/modules/marketing/classes/'.substr($class, 10).'.php';
		include_once($f);
	} elseif (__NAMESPACE__ == 'Admin') {
		$f = __DIR__.'/modules/admin/classes/'.$class.'.php';
		if (file_exists($f))
			include_once($f);
	} elseif (substr($class, 0, 6) == 'Admin\\' && substr($class, 0, 7) !== 'Admin\\j') {
		$f = __DIR__.'/modules/admin/classes/'.substr($class, 6).'.php';
		include_once($f);
	}
}

function p($value, $name='') {
	return \jLog::dump($value, $name);
}

function _t($name, $params=NULL, $empty_return=False, $lang=NULL) { 
	return Ry\Common::t($name, $params, $empty_return, $lang);
}

spl_autoload_register('ry_autoload');

