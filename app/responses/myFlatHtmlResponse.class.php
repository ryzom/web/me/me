<?php
/**
* @package   me
* @subpackage 
* @author    Ulukyn
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/


require_once (JELIX_LIB_CORE_PATH.'response/jResponseHtml.class.php');

class myFlatHtmlResponse extends jResponseHtml {

	public $bodyTpl = '';

	function __construct() {
		parent::__construct();

        $this->addMeta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']);
	}

	protected function doAfterActions() {

		$this->body->assignIfNone('MAIN','');
	}
}
