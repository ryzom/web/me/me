<?php
/**
* @package   me
* @subpackage 
* @author    Ulukyn
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/


require_once (JELIX_LIB_CORE_PATH.'response/jResponseHtml.class.php');

class myHtmlResponse extends jResponseHtml {

	public $bodyTpl = 'me~main';

	function __construct() {
		parent::__construct();
		// Include your common CSS and JS files here
		$this->addMeta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']);
	}

	protected function doAfterActions() {
		// Include all process in common for all actions, like the settings of the
		// main template, the settings of the response etc..
		$this->body->assignIfNone('MAIN','<p>no content</p>');
	}
}
