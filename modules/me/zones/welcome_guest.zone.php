<?php

/**
* @package    jelix-modules
* @subpackage jauth
* @author     Laurent Jouanneau
* @contributor Antoine Detante
* @copyright  2005-2006 Laurent Jouanneau, 2007 Antoine Detante
* @link       http://www.jelix.org
* @licence  http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public Licence, see LICENCE file
*/

class Welcome_guestZone extends jZone {
	protected $_tplname='welcome_guest';

	protected function _prepareTpl(){

		$this->_tpl->assign('username', $this->param('username'));
	}
}

