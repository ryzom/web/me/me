<?php
/**
* @package   me
* @subpackage me
* @author    Ulukyn
* @copyright 2020 Winchgate
* @link      me.ryzom.com
* @license    All rights reserved
*/

class profileCtrl extends Ry\UserController {
	protected $dbProfile = 'nel';
	protected $viewTemplate = 'me~profile';
	protected $editTemplate = 'me~profile_edit';
	protected $dao = 'api~nel__user';
	protected $form = 'me~profile';

	public function __construct($request) {
		parent::__construct($request);
		$this->moduleName = 'profile';
	}

	public function index() {
		$rep = $this->_getResponse();
		
		$user = $this->show_user;
		$id = $this->user->id;
		$show_user_id = 0;
		if ($this->show_user_id && is_numeric($this->show_user_id) && $this->user->checkAccess('account:show_profile')) {
			$id = intval($this->show_user_id);
			$show_user_id = $id;
		}
		$db = Ry\DB::spawn('nel');
		$temp_password = $db->getById('temp_admin_psswd', $user->id);
		
		$form = $this->_createForm($id);
		$form->initFromDao($this->dao, $id, $this->dbProfile);
		$form->setData('Community', $this->show_user->community);
		$tpl = new jTpl();
		$tpl->assign('id', $id);
		$tpl->assign('form', $form);
		$tpl->assign('show_user_id', $show_user_id);
		$tpl->assign('temp_password', $temp_password);
		$tpl->assign('show_user_name', $this->show_user->name);
		$tpl->assign('user_lang', $this->show_user->community);
		$tpl->assign('username', $form->getData('Login'));
		$bd = $form->getData('Birthday');
		if (strlen($bd) > 4 && $bd[4] != '-')
			$bd = substr($bd, 0, 4).'-'.substr($bd, 4, 2).'-'.substr($bd, 6);
		$form->setData('Birthday', $bd);
		$form->setData('Password', '*****');
		Ry\Common::setupMessage($rep);
		$rep->body->assign($this->templateAssign, $tpl->fetch($this->viewTemplate));
		return $rep;
	}

	public function update() {
		$rep = $this->getResponse('redirect');
		$rep->action='me~profile:index';
		
		$user = $this->show_user;
		if ($this->show_user_id) {
			$id = intval($this->show_user_id);
			$rep->params = ['show_user' => $this->show_user_id];
		} else
			$id = $this->user->id;
		
		if (!$id && $this->show_user_id) {
			jMessage::add('Edition denied...');
			return $rep;
		}
		
		$form = $this->_getForm($id);
		$form->initModifiedControlsList();
		try {
			$form->initFromRequest();
		} catch (Exception $e) {
			jMessage::add('error...');
			return $rep;
		}
		
		if (!$form->check()) {
				jMessage::add(_t('error_in_form'), 'error');
		} else {
			$changes = $form->getModifiedControls();
			if ($this->show_user_id && ($this->user->checkAccess('account:edit') || $this->user->checkAccess('account:set_temp_password'))) {
				$password =  $_POST['Password'];
				$error = $this->auth->checkPostValue('password', $password);
				if ($error) {
					$form->setData('Password', $user->infos->Password);
					if ($_POST['Password'] && $_POST['Password'] != '*****')
						jMessage::add($error);
				} else {
					$temp_pass_form = $this->_getForm($id);
					$db = Ry\DB::spawn('nel');
					$db->insertOrUpdate('temp_admin_psswd', ['IdUser' => $id, 'SecurePassword' => $user->infos->SecurePassword, 'GamePassword' => $user->infos->Password], ['IdUser' => $id]);
					$temp_pass_form->setData('Password', $user->getEncryptedPassword($_POST['Password']));
					$temp_pass_form->setData('SecurePassword', $user->getHashedPassword($_POST['Password']));
					jMessage::add('Temp Password Setuped');
					Ry\Common::log('profile', 'CS Set Temp Password', 'info', '', $id);
					$this->utils->sendRocket($this->user->name.' set Temp Password to '.$user->infos->Login.' ('.$user->infos->UId.')', 'z-command-logs', 'Splinter.Shell');
					$temp_pass_form->saveToDao($this->dao);
				}
				
				if ($this->user->checkAccess('account:edit')) {
					$form->saveToDao($this->dao);
					unset($changes['Password']);
					if ($changes) {
						Ry\Common::log('profile', 'CS Update', 'info', json_encode($changes), $id);
						jMessage::add(_t('profile_updated'));
					}
				} else if ($error) {
					jMessage::add('You can\'t edit this profile', 'error');
				}
			} else {
				if (!isset($_POST['Password'])) {
					jMessage::add(_t('passwords_dont_match'), 'error');
				} else if ($user->checkPassword($_POST['Password'])) {
					$form->setData('Password', $user->infos->Password);
					$form->setData('SecurePassword', $user->infos->SecurePassword);
					$old_email = $user->infos->Email;
					$new_email = isset($_POST['Email']) && $_POST['Email'] != $old_email?$_POST['Email']:'';
					if ($user->infos->EmailValidated)
						$form->setData('Email', $old_email);
					$form->saveToDao($this->dao);
					if ($new_email) {
						$user->askEmailValidation($new_email);
						Ry\Common::log('profile', 'email_ask_change', 'info', json_encode($changes));
					} else
						jMessage::add(_t('profile_updated'));
				} else {
					if ($this->show_user_id)
						jMessage::add('You can\'t edit this profile', 'error');
					else
						jMessage::add(_t('api~passwords_dont_match'), 'error');
				}
				unset($changes['Password']);
				if ($changes)
					Ry\Common::log('profile', 'update', 'info', json_encode($changes));
			}
		}
		return $rep;
	}

	public function set_back_original_password() {
		$rep = $this->getResponse('redirect');
		$rep->action='me~profile:index';
		
		$id = $this->show_user_id;
		$user = $this->show_user;
		if ($id) {
			$rep->params = ['show_user' => $id];
			$db = Ry\DB::spawn('nel');
			$temp_password = $db->getById('temp_admin_psswd', $id);
			if ($temp_password) {
				$db->delete('temp_admin_psswd', $id);
				$db->update('user', ['Password' => $temp_password->GamePassword, 'SecurePassword' => $temp_password->SecurePassword], ['UId' => $id]);
				jMessage::add('Temp Password Removed');
				Ry\Common::log('profile', 'CS Remove Temp Password', 'info', '', $id);
				$this->utils->sendRocket($this->user->name.' remove Temp Password to '.$user->infos->Login.' ('.$user->infos->UId.')', 'z-command-logs', 'Splinter.Shell');
			}
		}
		return $rep;
	}
}