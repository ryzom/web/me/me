<?php
/**
* @package   me
* @subpackage me
* @author    Ulukyn
* @copyright 2020 Winchgate
* @link      me.ryzom.com
* @license    All rights reserved
*/

class defaultCtrl extends Ry\Controller {

	private function getActiveVoucher() {
		return $this->ini['voucher']['active'];
	}


	function index() {
		$rep = $this->_getResponse('htmlauth');
		
		$rep->bodyTpl = 'main';
		
		$download = $this->param('download');
		if ($download) {
			$Browser = new foroco\BrowserDetection();
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$result = $Browser->getOS($useragent);
			if ($download == 'windows') {
				if ($result['64bits_mode'])
					$this->redirect('https://download.ryzom.com/ryztart_installer_64.exe');
				else
					$this->redirect('https://download.ryzom.com/ryztart_installer_32.exe');
			} elseif ($download == 'linux') {
				$rep = $this->getResponse('html');
				$rep->bodyTpl = '';
				$content = file_get_contents('https://me.ryzom.com/marketing/page?id=6&lang='.Ry\Common::getUserLang());
				$rep->addContent($content);
				header('refresh:0.1;url=https://download.ryzom.com/ryztart_installer_64.tgz');
				return $rep;
			} else
				$this->redirect('https://download.ryzom.com/ryztart_installer_osx.pkg');
			exit();
		}
		
		
		$login_errors = \jMessage::get('login_errors');
		if ($login_errors)
			$login_errors = implode('<br />', $login_errors);

		if ($this->auth->isLogged()) {
			$user = $this->show_user;
			$user_billing = $user->getBilling();
			$user_infos = $user->getBrief();
			
			$user_infos['subscription'] = $user->getSubscription();
			$user_infos['logs_fields'] = ['date', 'module', 'action', 'params', 'type', 'agent'];

			if ($this->show_user_id && !$this->user->checkAccess('account:transactions'))
				$user_infos['coupons'] = $user_infos['logs'] = [];
			else
				$user_infos['coupons'] = $user->getBillingHistory($user_infos['subscription']);

			$user_infos['logs'] = $user->getLogs();

			$voucher = $this->getActiveVoucher();

			if (isset($_SESSION[$voucher]) && $_SESSION[$voucher] && $voucher && !$user->getProviderTransactionIdForUser($user->id, $voucher)) {
				$user_infos['available_voucher'] = _t('me~freetime_voucher', [$_SESSION[$voucher]]);
				$user_infos['available_voucher_infos'] = str_replace("\n", '<br />', _t('billing~'.$voucher));
				$user_infos['available_voucher_url'] = jUrl::getFull('api~account:premium_free_days', ['voucher' => $voucher]);
				//$user_infos['available_voucher_url'] = '#';
			} else {
				$user_infos['available_voucher'] = '';
			}
 
			if ($this->show_user_id && !$this->user->checkAccess('account:show_profile')) {
				$email = explode('@', $user_infos['email']);
				$user_infos['email'] = substr($email[0], 0, 2).'***'.substr($email[0], strlen($email[0])-2, 2).'@'.$email[1];
			}

			if ($this->param('ask_email_validation') && $user_infos['validated'] != 'validated') {
				$user->askEmailValidation($user_infos['email']);
				if ($this->show_user_id)
					Ry\Common::log('profile', 'CS Ask Email Validation', 'info', '', $this->show_user_id);
				return $this->backHome();
			}

			$users = [$user_infos];
			$rep->body->assign('user', $user_infos);

			$other_accounts = isset($_SESSION['display_accounts'])?$_SESSION['display_accounts']:[];

			foreach($other_accounts as $account => $none) {
				$user = new Ry\Account($account);
				$user->updateRyzomInfos();
				$infos = $user->getBrief();
				$infos['coupons'] = $user->getBillingHistory($user_infos['subscription']);
				$users[] = $infos;
			}

			$form = \jForms::create('display_account');

			$billing_utils = Billing\Utils::spawn($this->ini, $user);
			$providers = $billing_utils->getProviders(@$user_billing->provider);
			$providers_prices = $billing_utils->getPrices(@$user_billing->provider, @$user_billing->months);
			
			$subs = new \jTpl();
			$subs->assign('providers', $providers);
			$subs->assign('prices', $providers_prices);
			$subs->assign('steamName', $user->steamName);
			$subs->assign('show_user_id', $this->show_user_id);
			$subs_html = $subs->fetch('me~subscriptions');
			
			$prices = [];
			$prices_total = [];
			$prices_dollar = [];
			
			$devises = explode("\n", Ry\Common::getFileContentsCached('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml', 60*60*24));
			foreach($devises as $line) {
				if (strpos($line, 'currency=\'USD\'') !== false)
					break;
			}
			
			$sline = explode('\'', $line);
			$conversion = floatval($sline[3]);

			$providers = ['XSolla' => 'xsolla_subscription', 'Paypal' => 'paypal', 'Steam' => 'steam'];
			$plans = ['m1', 'm3', 'm6', 'm12'];
			foreach($providers as $provider => $price_name) {
				foreach($plans as $plan) {
					$prices_total[$provider][$plan] = floatval($this->ini[$price_name.'_prices'][$plan]);
					$prices[$provider][$plan] = round($prices_total[$provider][$plan] / intval(substr($plan, 1)), 2);
					$prices_total_dollar[$provider][$plan] = round($prices_total[$provider][$plan] * $conversion, 2);
					$prices_dollar[$provider][$plan] = round($prices[$provider][$plan] * $conversion, 2);
				}
			}
			
			$daily_providers = ['XSolla' => 'xsolla'/*, 'Paypal' => 'paypal_short'*/];
			$daily_plans = ['d2', 'd3', 'd5', 'd7'];
			foreach($daily_providers as $provider => $price_name) {
				foreach($daily_plans as $plan) {
					$daily_prices_total[$provider][$plan] = floatval($this->ini[$price_name.'_prices'][$plan]);
					$daily_prices[$provider][$plan] = round($daily_prices_total[$provider][$plan] / intval(substr($plan, 1)), 2);
					$daily_prices_total_dollar[$provider][$plan] = round($daily_prices_total[$provider][$plan] * $conversion, 2);
					$daily_prices_dollar[$provider][$plan] = round($daily_prices[$provider][$plan] * $conversion, 2);
				}
			}
			
			$zp = [
				'SUBSCRIPTIONS' => $subs_html,
				'display_account_form' =>  $form,
				'user' => $user_infos,
				'accounts' => $users,
				'tr_plan' => [
					1 => _t('payment_m1'), 3 => _t('payment_m3'), 6 => _t('payment_m6'), 12 => _t('payment_m12'),
					'm1' => _t('payment_m1'), 'm3' => _t('payment_m3'), 'm6' => _t('payment_m6'), 'm12' => _t('payment_m12'),
					'd2' => _t('payment_d2'), 'd3' => _t('payment_d3'), 'd5' => _t('payment_d5'), 'd7' => _t('payment_d7'),
				],

				'providers' => $providers,
				'plans' => $plans,
				'prices' => $prices,
				'prices_total' => $prices_total,
				'prices_dollar' => $prices_dollar,
				'prices_total_dollar' => $prices_total_dollar,

				'daily_providers' => $daily_providers,
				'daily_plans' => $daily_plans,
				'daily_prices' => $daily_prices,
				'daily_prices_total' => $daily_prices_total,
				'daily_prices_dollar' => $daily_prices_dollar,
				'daily_prices_total_dollar' => $daily_prices_total_dollar,
				'show_user_id' => $this->show_user_id,
				];
		
			$rep->body->assignZone('MAIN', 'me~welcome', $zp);
		} else {
			$rep->addJSLink('https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer');
			$rep->body->assign('username', '');
			$rep->body->assign('user_id', '');
			
			$rep->body->assign('user', ['groups' => []]);
			$form_vals = \jMessage::get('form-values');
			if ($form_vals)
				$form_vals = json_decode($form_vals[0], true);
			else
				$form_vals = ['', ''];
			
			$scope = $this->param('scope');
			if (!$scope) {
				if ($this->param('language'))
					$scope = 'client';
				else
					$scope = 'web';
			}
			
			$zp = [
				'lang' => Ry\Common::getUserLang(),
				'messages' => '',
				'module' => \jApp::coord()->request->params['module'],
				'captcha_site_key' => $this->ini['auth']['captcha_key'],
				'login_errors' => $login_errors,
				'forget_password' => $this->param('forget_password') == 1,
				'from' => 'https://'.$this->request->getDomainName().($this->request->getPort() != 443?$this->request->getPort():''),
				'form_vals' => $form_vals,
				'form_id' => $this->param('me_form_id', $_SESSION['me_form_id']),
				'scope' => $scope];
			$rep->body->assignZone('MAIN', 'me~welcome_guest', $zp);
			$rep->body->assign('show_user_name', 'Guest');

		}
		Ry\Common::setupMessage($rep);
		return $rep;
	}

	public function displayAccount() {
		$rep = $this->getResponse('redirect');
		$rep->action='me~default:index';

		$user = Ry\Account::spawn();
		if (!$user->checkAccess('account:search')) {
			\jMessage::add('no_soup_for_you');
			return $rep;
		}

		if (!isset($_SESSION['display_accounts']))
			$_SESSION['display_accounts'] = [];

		$form = jForms::fill('display_account');
		if (!$form->check()) {
			\jMessage::add('error_in_form');
			return $rep;
		}

		$type = $form->getData('type');
		$value = $form->getData('value');
		if ($type == 'uid') {
			$_SESSION['display_accounts'][$value] = true;
		}

		return $rep;
	}

	public function hideAccount() {
		$rep = $this->getResponse('redirect');
		$rep->action='me~default:index';

		$user = Ry\Account::spawn();
		if (!$user->checkAccess('account:search')) {
			\jMessage::add('no_soup_for_you');
			return $rep;
		}
		
		$uid = $this->param('uid');
		unset($_SESSION['display_accounts'][$uid]);

		return $rep;
	}
	
	public function send_gift() {
		$rep = $this->getResponse('redirect');
		$rep->action='me~default:index';

		if ($this->auth->isLogged()) {
			$id = $this->param('id');
			$token = $this->param('token');
			$validate = $this->param('validate');
			$player_name = $this->param('player_name');
			$from_character = $this->param('from_character');
			$gift_message = $this->param('gift_message');
			
			if (isset($_SESSION['me_send_gifts'][$id]) && $_SESSION['me_send_gifts'][$id] == $token) {
				$user = Ry\Account::spawn();
				$db = Ry\DB::spawn('billing~billing');
				$transaction = $db->getSingle('transactions', ['user_id' => $user->id, 'id' => $id]);
				if ($transaction) {
					$user_billing = $user->getBilling();
					$end_sub = new \DateTime($user_billing->end_date);
					$today = new \DateTime('today midnight');
					$add_sub = new \DateTime('today midnight');
					$add = DateInterval::createFromDateString(substr($transaction->plan, 1).' months');
					$add_sub->add($add);
					$add_sub_duration = $add_sub->diff($today);
					$end_sub_duration = $end_sub->diff($today);
					$days_to_add = min($add_sub_duration->days, $end_sub_duration->days);
					if ($days_to_add == 0) {
						$rep = $this->_getResponse('html');
						\Ry\Common::setupMessage($rep);
						$tpl = new \jTpl();
						$tpl->assign('title', _t('no_enough_days'));
						$html = $tpl->fetch('api~process_error');
						$rep->body->assign('MAIN', $html);
						return $rep;
					}
					
					$db_live = jDb::getConnection('ring_live');
					$tpl = new \jTpl();
					if ($validate) {
						$rep = $this->_getResponse('html');
						$char_infos = $db_live->query('SELECT * FROM `characters` WHERE char_name='.$db_live->quote($player_name));
						$char = $char_infos->fetch();
						if ($char) {
							$account_player = $db->getSingle('billing_accounts', ['user_id' => $char->user_id]);
							$nbr_days = DateInterval::createFromDateString($days_to_add.' days');
							$db->update('transactions', ['user_id' => $char->user_id, 'date' => date('Y-m-d H:m:i'), 'plan' => 'd'.$days_to_add, 'status' => 'GIFT'], ['id' => $id]);
							Ry\Common::log('payment', 'gift', 'info', $days_to_add.' to '.$char->name,  $user->id);
							$end_sub->sub($nbr_days);
							$db->update('billing_accounts', ['end_date' => $end_sub->format('Y-m-d')], ['user_id' => $user->id]);
							p($account_player);
							if ($account_player && $account_player->provider == 'worldpay') {
								$tpl->assign('title', _t('player_cant_receive_gift'));
								$html = $tpl->fetch('api~process_error');
							} elseif ($account_player && $account_player->provider == 'xsolla_subscription') {
								$dst_user = Ry\Account::spawn($char->user_id);
								$gateway = new \Billing\XsollaSub($this->ini, $dst_user);
								$gateway->_init();
								$gateway->postponeSubscription($days_to_add);
							} else {
								if ($account_player) {
									$dst_end_date = new \DateTime($account_player->end_date);
									if ($dst_end_date > $today)
										$today = $dst_end_date;
									$today->add($nbr_days);
									$db->update('billing_accounts', ['provider' => 'gift', 'plan' => 'd'.$days_to_add, 'days' => $days_to_add, 'months' => 0, 'end_date' => $today->format('Y-m-d')], ['user_id' => $char->user_id]);
								} else {
									$today->add($nbr_days);
									$db->insert('billing_accounts', ['provider' => 'gift', 'plan' => 'd'.$days_to_add, 'days' => $days_to_add, 'months' => 0, 'end_date' => $today->format('Y-m-d'), 'user_id' => $char->user_id]);
								}
							}
							$tpl->assign('title', _t('gift_sent'));
							$html = $tpl->fetch('api~process_ok');
							$this->utils->sendRocket($user->name.'('.$user->id.') send '.$days_to_add.' days of sub to '.$player_name.' ('.$char->user_id.')', 'ulukyn-billing-logs', 'Splinter.Shell');
							
							$db_nel = Ry\DB::spawn('api~nel');
							$user_infos = $db_nel->getSingle('user', ['UId' => $char->user_id]);
							$dest_user = \Ry\Account::spawn($char->user_id);
							$dest_lang = $dest_user->infos->Community;
							$langs = \Ry\Common::getLangs();
							if (!isset($langs[$dest_lang]))
								$dest_lang = 'en';
							
							$wkr = new jWiki('markdown_to_xhtml');
							$title = _t('billing~email_title_gift_sent', NULL, False, $dest_lang);
							$message = _t('billing~email_message_gift_sent', NULL, False, $dest_lang);
							$subject = $wkr->render(str_replace($title, '%dest%', ucfirst($user_infos->Login)));
							$text = $wkr->render(str_replace(
								['%dest%', '%number%', '%character%', '%text%'],
								[ucfirst($user_infos->Login), $days_to_add, $from_character, $gift_message],
								$message));
							$this->utils->sendIGEmail('#'.$char->user_id, $subject, $text, 'Me - Account');
							$this->utils->sendEmailWithTemplate($title,
								'api~mail_content_generic',
								$text, 
								$values=['bgcolor' => 'orange', 'color' => 'black', 'title' => $title,
									'dest' => ucfirst($user_infos->Login),
									'number' => '<strong style="color: cyan">'.$days_to_add.'</strong>',
									'character' => '<strong style="color: orange">'.$from_character.'</strong>',
									'infos' => $text,
									'text' => '<strong style="color: yellow">'.$gift_message.'</strong>',
								],
								$char->user_id);
						} else {
							$tpl->assign('title', _t('player_not_found'));
							$html = $tpl->fetch('api~process_error');
						}
						
						$rep->body->assign('MAIN', $html);
						\Ry\Common::setupMessage($rep);
					} else {
						$rep = $this->_getResponse('htmlauth');
						$rep->bodyTpl = 'main';
						$form = jForms::get('send_gift');
						$create_form = $form === Null;
						if ($form !== Null) {
							try {
								$form = \jForms::fill('send_gift');
							} catch(Exception $e) {
								$create_form = true;
							}
						}
						
						if ($create_form) {
							$form = \jForms::create('send_gift');
							$form->setData('id', $id);
							$form->setData('token', $token);
							$player_name = '';
						} else {
							$player_name = $this->param('player_name');
							$error_message =  _t('player_not_found');
							$char_infos = $db_live->query('SELECT * FROM `characters` WHERE char_name='.$db_live->quote($player_name));
							$char = $char_infos->fetch();
							if ($char) {
								$account_player = $db->getSingle('billing_accounts', ['user_id' => $char->user_id]);
								if ($account_player && $account_player->provider == 'worldpay') {
									$error_message = _t('player_cant_receive_gift');
									$char = Null;
								}
							}
							
							if (!$char) {
								$tpl->assign('title', $error_message);
								$html = $tpl->fetch('api~process_error');
								$rep->body->assign('MAIN', $html);
								\Ry\Common::setupMessage($rep);
								return $rep;
							} else {
								$from_char = $this->param('char');
								$message = str_replace("\n", '<br />', htmlentities($this->param('message')));
							}
						}

						$datasource = new jFormsStaticDatasource();
						$chars = \Ry\DB::query('ring_live', 'SELECT char_name, char_id FROM characters WHERE user_id = %s ORDER BY char_id', [$user->id]);
						$form_chars =  [_t('billing~a_friend') => _t('billing~a_friend')];
						foreach($chars as $char)
							$form_chars[ucfirst($char->char_name)] = ucfirst($char->char_name);
						if ($form_chars) {
							$datasource->data = $form_chars;
							$control = $form->getControl('char');
							if ($control)
								$control->datasource = $datasource;
						}

						$zp = [
							'player_name' => $player_name,
							'from_char' => $from_char,
							'message' => $message,
							'days' => $days_to_add,
							'id' => $id,
							'token' => $token,
							'form' => $form,
							];
						Ry\Common::setupMessage($rep);
						$rep->body->assignZone('MAIN', 'me~send_gift', $zp);
					}
				} else {
					$rep = $this->_getResponse('html');
					\Ry\Common::setupMessage($rep);
					$tpl = new \jTpl();
					$tpl->assign('title', _t('no_transaction'));
					$html = $tpl->fetch('api~process_error');
					$rep->body->assign('MAIN', $html);
					return $rep;
				}
			} else {
				$rep = $this->_getResponse('html');
				\Ry\Common::setupMessage($rep);
				$tpl = new \jTpl();
				$tpl->assign('title', _t('bad_token'));
				$html = $tpl->fetch('api~process_error');
				$rep->body->assign('MAIN', $html);
				return $rep;
			}
		}
		return $rep;
	}
}
