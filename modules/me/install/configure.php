<?php
/**
* @package   me
* @subpackage me
* @author    Ulukyn
* @copyright 2019 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/


class meModuleConfigurator extends \Jelix\Installer\Module\Configurator {

    public function getDefaultParameters() {
        return array();
    }

    function configure(\Jelix\Installer\Module\API\ConfigurationHelpers $helpers) {

    }
}