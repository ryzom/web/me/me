<div id="prices" class="badge-grey big-frame">
	<table width="100%">
	<tr>
		{foreach $providers as $provider => $value}
		<th colspan="2">{$provider}</th>
		{/foreach}
	</tr>

	{foreach $plans as $plan}
	<tr{if $plan == 'm1'} class="padding"{/if}> 
		{foreach $providers as $provider => $value}
			{if $prices_total[$provider][$plan]}
			<td align="right" class="bold">{t 'me~payment_'.$plan} :&nbsp;</td><td>&nbsp;<span class="{if $plan == 'm1'}orange{/if} bold">{$prices_total[$provider][$plan]}€</span> (&asymp;${$prices_total_dollar[$provider][$plan]})</td>
			{/if}
		{/foreach}
	</tr>
	{if $plan != 'm1'}
	<tr class="padding-b">
		{foreach $providers as $provider => $value}
			{if $prices[$provider][$plan]}
			<td align="right" class="bold"></td><td>&nbsp;<span class="orange bold">{$prices[$provider][$plan]}€</span>/{t 'me~month'} (&asymp;${$prices_dollar[$provider][$plan]})</td>
			{/if}
		{/foreach}
	</tr>
	{/if}
	{/foreach}
	
	<tr><td></td></tr>
	{foreach $daily_plans as $plan}
	<tr{if $plan == 'd1'} class="padding"{/if}> 
		{foreach $daily_providers as $provider => $value}
		{if $daily_prices_total[$provider][$plan]}
			<td align="right" class="bold">{t 'me~payment_'.$plan} :&nbsp;</td><td>&nbsp;<span class="{if $plan == 'd1'}orange{/if} bold">{$daily_prices_total[$provider][$plan]}€</span> (&asymp;${$daily_prices_total_dollar[$provider][$plan]})</td>
		{/if}
		{/foreach}
	</tr>
	{if $plan != 'd1'}
	<tr class="padding-b">
		{foreach $daily_providers as $provider => $value}
			{if $daily_prices[$provider][$plan]}
			<td align="right" class="bold"></td><td>&nbsp;<span class="orange bold">{$daily_prices[$provider][$plan]}€</span>/{t 'me~day'} (&asymp;${$daily_prices_dollar[$provider][$plan]})</td>
			{/if}
		{/foreach}
	</tr>
	{/if}
	{/foreach}
	</table>
</div>
