<h3 class="padding-5">{t 'gift_to_player'}</h3>
{if $player_name}
{include 'me~_messages'}
<div class="big-frame account">
	<h5 class="orange">{t 'number_days_to_send_as_gift'} <span class="cyan">{$days}</span></h5>
	<h5 class="orange">{t 'player_to_send_the_gift'} <span class="cyan">{$player_name}</span></h5>
	<div class="gift-message box-frame 50">
		<h5 class="white ">{$message}</h5>
		<h5 class="yellow float-right">- {$from_char}</h5>
		<br />
	</div>
		<div class="clear"></div>
	<a class="form-submit" href="{ryurl 'me~default:send_gift', ['validate' => 1, 'id' => $id, 'token' => $token, 'gift_message' => $message, 'from_character' => $from_char, 'player_name' => $player_name ]}">{t 'validate'}</a>
	<div class="clear"></div>
</div>
{else}
	{form $form, 'me~default:send_gift'}
	<div class="big-frame account">
		<h5>{t 'number_days_to_send_as_gift'} <span class="cyan">{$days}</span></h5>
		<fieldset>
			<table class="form">
				{formcontrols}
				<tr><td class="form-label">{t_ctrl_label}</td><td>{ctrl_control}</td></tr>
				{/formcontrols}
			</table>
		</fieldset>
			<a href="#" onclick="document.getElementById('jforms_me_send_gift').submit(); return false;"><div class="form-submit">{t 'submit'}</div></a>
		<div class="clear"></div>
	</div>
	{/form}
{/if}

