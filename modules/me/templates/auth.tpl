
<div class="container slim pt-5 pb-5 text-center">
	<h1>Please Authenticate</h1>

	<p>
	{$MAIN}
	</p>

	<form class="text-left" action="/oauth/authenticate" method="post">
		<input type="hidden" name="from" value="{$from}">
		<input type="hidden" name="scope" value="{$scope}">
		<div class="form-group">
			<label for="account_name">Ryzom Account Name</label>
			<input type="text" name="username" class="form-control" id="account_name" placeholder="Enter account name">
		</div>
		<div class="form-group">
			<label for="password_input">Password</label>
			<input type="password" name="password" class="form-control" id="password_input" placeholder="Password">
		</div>
		<button type="submit" class="btn btn-primary btn-block font-large text-uppercase">Login</button>
	</form>
	{if $create_account}
	<h2 class="mt-4 mb-4">OR</h2>
	<p>
		<a href="#" class="btn btn-secondary font-large" style="margin: 5px"><i class="fas fa-envelope fa-2x"></i> Create Ryzom Account</a>
	</p>
	{/if}
</div>
