<div id="prices" class="badge-grey big-frame">
	<table width="100%">
	<tr>
		{foreach $providers as $provider => $value}
		<th colspan="2">{$provider}</th>
		{/foreach}
	</tr>

	{foreach $plans as $plan}
	<tr class="padding-b">
		{foreach $providers as $provider => $value}
			<td align="right" class="bold">{t 'me~payment_'.$plan} :&nbsp;</td><td class="cell-with-comment"><span class="cell-comment">&nbsp;{$prices_total[$provider][$plan]}€ <span class="white"> (&asymp;${$prices_total_dollar[$provider][$plan]})</span> </span>&nbsp;<span class="orange bold">{$prices[$provider][$plan]}€</span> / {t 'me~month'} (&asymp;${$prices_dollar[$provider][$plan]})</td>
		{/foreach}
	</tr>
	{/foreach}
	
	<tr bgcolor="#555">
		{foreach $providers as $provider => $value}
			<td></td><td></td>
		{/foreach}
	</tr>
	{foreach $daily_plans as $plan}
	<tr class="padding-b">
		{foreach $daily_providers as $provider => $value}
			<td align="right" class="bold">{t 'me~payment_'.$plan} :&nbsp;</td><td>&nbsp;<span class="orange bold">{$daily_prices[$provider][$plan]}€</span> / {t 'me~day'} (&asymp;${$daily_prices_dollar[$provider][$plan]})</td>
		{/foreach}
	</tr>
	{/foreach}
	</table>
</div>

