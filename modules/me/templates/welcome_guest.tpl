<script>
	{if $lang == 'en'}
	const langSlug = ''
	{else}
	const langSlug = '/{$lang}'
	{/if}

	{literal}
	function goToDownloadpage() {
		document.location.href = 'https://ryzom.com' + langSlug + '/download';
	}
	function goToTos() {
		const a = document.createElement('a');
		a.href = 'https://ryzom.com' + langSlug + '/terms-of-service';
		a.target = '_blank'

		a.click();
	}
	function goToPrivacyPolicy() {
		const a = document.createElement('a');
		a.href = 'https://ryzom.com' + langSlug + '/privacy';
		a.target = '_blank'

		a.click();
	}
	{/literal}
</script>

<style>
	{literal}
	#center {
		margin-top: 0 !important;
	}
	{/literal}
</style>

<!--<div class="d-none d-md-block pt-4 pb-4 pt-md-5 pb-md-5">
	<div class="container"><h1>Account / Subscription</h1></div>
</div>-->

<div class="bg-dark section-border-bottom"
	 style="background-image: url('/images/bg_create_account.jpg'); background-size: cover; background-position: center center;">
	<div class="container extended pt-5 pb-5">
		<div class="row">
			<div class="col-md-6 col-xl-3">
				<div class="row h-100">
					<div class="col-md-6 col-xl-12" style="min-width: 100%">
						<div class="text-center text-decoration-none h-100 text-link-color ryzom-framed p-4 effect-enlarge" style="background: rgba(0, 0, 0, 0.9);">
							<div id="log-in">
								{zone 'me~login', ['login_errors' => $login_errors, 'from' => '/', 'scope' => 'ryzom', 'action' => '/api/auth/ryzom_authenticate']}
							</div>
							<div id="forget-password" class="hidden">
								{include 'me~_box_password'}
							</div>
							
						</div>
					</div>
					<div id="donate-box">
						<div class="text-center text-decoration-none text-link-color p-4 effect-enlarge">
							<a href="/billing/donate"><img src="/images/donate.png" height="92px" /></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-xl-6 mt-4 mt-md-0">
				<div class="text-center ryzom-framed p-4 h-100 effect-enlarge text-decoration-none text-link-color"
					 style="background: rgba(0, 0, 0, 0.9);">
					<!--<div id="reg-promo">
						{include 'me~_box_join'}
					</div>
					<div id="reg-form" class="hidden">-->
						{zone 'me~register', ['scope' => $scope, 'form_id' => $form_id, 'message' => $messages, 'form_vals' => $form_vals, 'captcha_site_key' => $captcha_site_key, 'action' => '/api/auth/ryzom_create_account']}
					<!--</div>-->
				</div>
			</div>
			<!--<div class="col-md-6 col-xl-3 mt-4 mt-xl-0">
				<div class="text-center text-decoration-none cursor-pointer h-100 text-link-color ryzom-framed p-4 effect-enlarge" onclick="goToDownloadpage()"
					 style="background: rgba(0, 0, 0, 0.9);">
					{include 'me~_box_download'}
				</div>
			</div>-->
			<div class="col-12 col-xl-3 mt-4 mt-xl-0">
				<div class="row h-100">
					<div class="col-md-6 col-xl-12">
						<div class="text-center text-decoration-none h-100 text-link-color ryzom-framed p-4 effect-enlarge"
							 style="background: rgba(0, 0, 0, 0.9);">
							{include 'me~_box_download'}
						</div>
					</div>

					<div class="col-md-6 col-xl-12 mt-4 mt-md-0 mt-xl-4">
						<div class="text-center ryzom-framed p-4 h-100 effect-enlarge text-decoration-none text-link-color"
							 style="background: rgba(0, 0, 0, 0.9);">
							{include 'me~_box_help'}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<ryzom-me lang="{$lang}"></ryzom-me>

<script>
	{if $forget_password}
	showPasswordRecoveryBox();
	{/if}
</script>