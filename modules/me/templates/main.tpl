<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MSQFC3Z" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="theApp">
	<header style="z-index: 1;">
		<ryzom-header id="theHeader" lang="{$lang}" enable-languages></ryzom-header>
		<script>
			{literal}
			function updateQueryStringParameter(uri, key, value) {
				var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
				var separator = uri.indexOf('?') !== -1 ? "&" : "?";
				if (uri.match(re)) {
					return uri.replace(re, '$1' + key + "=" + value + '$2');
				}
				else {
					return uri + separator + key + "=" + value;
				}
			}

			document.getElementById("theHeader").addEventListener('change-lang', (event) => {
				const currentLocation = location.href;

				location.href = updateQueryStringParameter(currentLocation, 'lang', event.detail);
			});
			{/literal}
		</script>
	</header>

	
	<div id="theContent" class="d-flex flex-column flex-md-row bg-black container extended p-0">
		<div id="menu" class="extended">
			{include 'me~_menu'}
		</div>
		<div id="center" class="bg-black">
			{include 'me~_messages'}
			{$MAIN}
		</div>
	</div>


	<footer>
		<ryzom-footer id="theFooter" lang="{$lang}" low-padding></ryzom-footer>
	</footer>

	<script>
		{literal}
		function detectIE() {
			const ua = window.navigator.userAgent;

			// Test values; Uncomment to check result …

			// IE 10
			// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

			// IE 11
			// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

			// Edge 12 (Spartan)
			// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

			// Edge 13
			// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

			const msie = ua.indexOf('MSIE ');
			if (msie > 0) {
				// IE 10 or older => return version number
				return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			}

			const trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				const rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}

			const edge = ua.indexOf('Edge/');
			if (edge > 0) {
				// Edge (IE 12+) => return version number
				return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
			}

			// other browser
			return false;
		}

		const version = detectIE();

		if (version === false) {
			document.getElementsByTagName('body')[0].classList.add('not_ms');
		} else if (version > 11) {
			document.getElementsByTagName('body')[0].classList.add('is_edge');
		} else {
			document.getElementsByTagName('body')[0].classList.add('is_ie');
		}
		{/literal}
	</script>
</div>