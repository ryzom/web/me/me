<script>
    {literal}
    function showPasswordRecoveryBox() {
        document.getElementById('recover_pass_account').value = document.getElementById('account_name').value;
        hide('log-in');
        show('forget-password');
    }
    {/literal}
</script>
<h2 class="text-uppercase">
    {t 'me~box_login_title_line_1'}
</h2>
<div class="font-large text-white">
    <form class="text-center" action="{$action}" method="post">
        <input type="hidden" name="from" value="{$from}">
        {if $login_errors}
            <div class="alert alert-danger">{$login_errors}</div>
        {/if}
        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~account_name'}
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="text"
                       name="username"
                       placeholder="{t 'api~account_name'}"
                       autocomplete="username"
                       class="d-inline-block hide-placeholder-xl form-control"
                       id="account_name"
                       style="max-width: 225px;">
            </div>
        </fieldset>
        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~password'}
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="password"
                       name="password"
                       placeholder="{t 'api~password'}"
                       autocomplete="current-password"
                       class="d-inline-block hide-placeholder-xl form-control"
                       style="max-width: 225px;">
            </div>
        </fieldset>
        <div class="text-center">
            <button type="submit" class="btn text-uppercase btn-secondary w-100" style="max-width: 225px;">{t 'button_login'}</button>
        </div>
        <div class="text-center mt-2">
            <div class="btn text-uppercase btn-link btn-sm w-100" style="max-width: 225px;" onclick="showPasswordRecoveryBox()">{t 'forget_password'}</div>
        </div>
    </form>
</div>