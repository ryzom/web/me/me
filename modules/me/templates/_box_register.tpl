<h2 class="text-uppercase">
    {t 'me~box_create_account_title_line_1'} {t 'me~box_create_account_title_line_2'}
</h2>
<div class="font-large text-white">
    <form class="text-center" id="{$form_id}" action="{$action}" method="post">
        <input type="hidden" name="use_form" value="1" />
        <input type="hidden" name="scope" value="{$scope}" />
        <input type="hidden" name="me_form_id" value="{$form_id}" />
        
        {if isset($message) && $message}
            <div class="alert alert-danger">{$message}</div>
        {/if}

        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~account_name'}
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="text" name="username" autocomplete="off" value="{$form_vals[0]}"
                       class="d-inline-block hide-placeholder-xl form-control" id="account_name"
                       placeholder="{t 'api~account_name'}" style="max-width: 304px;">
            </div>
        </fieldset>

        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~email'}
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="email" name="email" autocomplete="email" value="{$form_vals[1]}"
                       class="d-inline-block hide-placeholder-xl form-control" id="email_input"
                       placeholder="{t 'api~email'}" style="max-width: 304px;">
            </div>
        </fieldset>

        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~password'} (<span style="color: lightgreen">&~#{ldelim}{rdelim}[]()-|_@!:.,;</span>)
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="password" name="password" autocomplete="new-password"
                       class="d-inline-block hide-placeholder-xl form-control" id="password_input"
                       placeholder="{t 'api~password'}" style="max-width: 280px;">
                <i class="fas fa-eye pointer" onclick="showPassword(this, 'password_input');"></i>
            </div>
        </fieldset>

        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~confirm_password'}
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="password" name="confirm_password" autocomplete="new-password"
                       class="d-inline-block hide-placeholder-xl form-control" id="confirm_password_input"
                       placeholder="{t 'api~confirm_password'}" style="max-width: 280px;">
                <i class="fas fa-eye pointer" onclick="showPassword(this, 'confirm_password_input');"></i>
            </div>
        </fieldset>

        <div class="text-center" style="z-index: 1;position: relative;">
            <div class="g-recaptcha p-0 d-inline-block" data-sitekey="{$captcha_site_key}"></div>
        </div>

        <div class="mt-2">
            <div class="d-inline-block" style="max-width: 404px;">
                {t 'me~box_create_account_agree_before'} <a href="#" onclick="goToPrivacyPolicy(); return false;">{t 'me~box_create_account_agree_privacy'} <i class="fas fa-external-link-alt"></i></a> {t 'me~box_create_account_agree_and'} <a href="#" onclick="goToTos(); return false;">{t 'me~box_create_account_agree_tos'} <i class="fas fa-external-link-alt"></i></a> {t 'me~box_create_account_agree_after'}
            </div>
        </div>

        <button type="submit" class="btn btn-secondary text-uppercase mt-2 w-100" style="max-width: 304px;">{t 'create_account'}</button>
    </form>
</div>