<div id="billing-options" class="centered">

	<div id="premium" class="text-center margin-box-10">
		<h4 class="primary-badge">{t 'benefits_of_premium'}</h4>
		<div id="premium-benefits" class="text-left">
			<p><i class="fas fa-sort-amount-up"></i> {t 'no_level_limit'}</p>
			<p><i class="fas fa-tshirt"></i> {t 'no_item_restriction'}</p>
			<p><i class="fas fa-luggage-cart"></i> {t 'all_storage_available'}</p>
			<p><i class="fas fa-dna"></i> {t 'double_xp'}</p>
		</div>
	</div>

	<div class="min250 text-center margin-box-10 effect-enlarge">
		<h2 class="title-frame badge-secondary">{t 'select_an_option'}</h2>
		<div class="box-frame text-left">
		{foreach $providers as $provider => $icon}
			<div id="{$provider}-tooltip" class="hidden">{t $provider.'-tooltip'}</div>
			<p><a href="#" id="{$provider}-link" onmouseover="showPaymentTooltip('{$provider}', '#4BD032')" onmouseleave="showPaymentTooltip()" onclick="selectPaymentOption('{$provider}'); return false;" class="img-link {$provider}-link" style="margin: 5px"><i class="{$icon}"></i></i><strong> {t $provider.'_payment'}{if $provider == 'steam' && !$show_user_id} [{$steamName}]{/if}</strong></a></p>
		{/foreach}
		</div>
	</div>

	<div id="providers" class="hidden">
	{foreach $providers as $provider => $icon}
		<div id="{$provider}" class="fadeIn hidden min250 text-center margin-box-10  effect-enlarge">
			<h2 class="title-frame badge-secondary">{t 'choice_a_length'}</h2>
			<div id="{$provider}-prices" class="box-frame text-left">
			{foreach $prices[$provider] as $id => $value}
				<a href="#" id="{$provider}-{$id}-link" class="img-link block padding-5" onclick="selectPayment('{$id}', '{$provider}'); return false;" style="margin: 5px"><i class="{if !is_numeric($value)}{$value}{else}fas fa-calendar-week{/if}"></i> <strong>{t 'payment_'.$id} <span class="price">{if is_numeric($value)}{$value}€{/if}</span></strong></a>
			{/foreach}
			</div>
		</div>
	{/foreach}
	</div>
</div>

<div id="payment-tooltip-container" class="bold padding-5"><span id="payment-tooltip">{t 'remove_limitations'}</span><span id="payment-tooltip-option"></span><span id="payment-tooltip-length"></span></div>
<div id="payment-tooltip-default" class="hidden">{t 'remove_limitations'}</div>
<div id="payment-box"class="fadeIn text-center margin-box-10">
	<div id="payment" class="hidden">
		<div id="payment-waiting"><img  height="64px" width="90px" src="/images/waiting.gif" /></div>
		<div id="payment-content" class="padding-5 text-center"></div>
		<div id="payment-content-error" class="hidden">{t 'unknow_error_retry'}</div>
	</div>
</div>
