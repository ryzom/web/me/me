{jmessage} 
<h3 class="padding-5">{t 'personal_data'}{if $show_user_id} <span class="orange">({$show_user_name}){/if}</span></h3>
{form $form, 'me~profile:update', ['show_user' => $show_user_id]}
<div class="big-frame account">
	<fieldset>
		<table class="form">
		{formcontrols}
		{ifctrl 'Newsletter'}
			<tr><td class="form-label">{t_ctrl_label}</td><td><label class="switch">{ctrl_control} <span class="slider round"></span></label></td></tr>
		{else}
			{ifctrl 'Password'}
				{if $show_user_id}
				<tr><td class="form-label">{t 'set_temporary_password'}</td><td>{ctrl_control 'Password', ['onfocus' => 'this.type=\'password\'; this.value=\'\'']} &nbsp;
					{if $temp_password}
						<a href="{ryurl 'me~profile:set_back_original_password', ['show_user' => $show_user_id]}" class="button rounded">{t 'put_back_original_password'}</a></td></tr>
					{else}
						<a href="{ryurl 'api~auth:ask_change_password', ['show_user' => $show_user_id, 'lang' => $user_lang]}" class="button rounded">{t 'send_password_change_email'}</a>
					{/if}
				{else}
				<tr><td class="form-label">{t 'enter_password_to_validate'}</td><td>{ctrl_control 'Password', ['onfocus' => 'this.type=\'password\'; this.value=\'\'']} &nbsp; <a href="{ryurl 'api~auth:ask_change_password'}" class="button rounded">{t 'send_password_change_email'}</a></td></tr>
				{/if}
			{else}
				<tr><td class="form-label">{t_ctrl_label}</td><td>{ctrl_control}</td></tr>
			{/ifctrl}
		{/ifctrl}
		{/formcontrols}
		</table>
	</fieldset>

	<a href="#" onclick="document.getElementById('jforms_me_profile').submit(); return false;"><div class="form-submit">{t 'Save'}</div></a>
	<div class="clear"></div>
</div>
{/form}

