<script>
    {literal}
    function showLoginBox() {
        document.getElementById('account_name').value = document.getElementById('recover_pass_account').value;
        hide('forget-password');
        show('log-in');
    }
    {/literal}
</script>

<h2 class="text-uppercase">
    {t 'me~box_recover_password_title_line_1'}<br />{t 'me~box_recover_password_title_line_2'}
</h2>
<div class="font-large text-white">
    <form class="text-center" action="/api/auth/recover_password" method="post">
        <fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~account_name'}
            </legend>
            <div tabindex="-1" role="group" class="bv-no-focus-ring">
                <input type="text" name="username" autocomplete="off"
                       class="d-inline-block hide-placeholder-xl form-control" id="recover_pass_account"
                       placeholder="{t 'api~account_name'}" style="max-width: 225px;">
            </div>
        </fieldset>
        <!--<fieldset class="form-group">
            <legend tabindex="-1" class="bv-no-focus-ring col-form-label pt-0 d-none d-xl-block">
                {t 'api~email'}
            </legend>
            <input type="email" name="email" autocomplete="off" class="d-inline-block hide-placeholder-xl form-control"
                   id="email_input" placeholder="{t 'api~email'}" style="max-width: 225px;">
        </fieldset>-->

        <div class="text-center">
            <button type="submit" class="btn btn-secondary text-uppercase">{t 'recover_password'}</button>
        </div>
        <div class="text-center mt-2">
            <div class="btn btn-link text-uppercase btn-sm" onclick="showLoginBox()">{t 'sign_in'}</div>
        </div>
    </form>
</div>