<h2 class="text-uppercase">
	{t 'me~box_download_title_line_1'}<br/>{t 'me~box_download_title_line_2'}
</h2>

<div class="d-flex align-items-center justify-content-center flex-column flex-sm-row" style="font-size: 3rem;">
	<span>
		<a id="{$button_id_prefix}windows" href="https://me.ryzom.com/?download=windows" target="_self" class="text-white effect-hover-color-link">
			<i class="fab fa-windows ml-2 mr-2"></i>
		</a>
	</span>

	<span>
		<a id="{$button_id_prefix}mac" href="https://me.ryzom.com/?download=mac" target="_self" 	class="text-white effect-hover-color-link">
			<i class="fab fa-apple ml-2 mr-2"></i>
		</a>
	</span>

	<span>
		<a id="{$button_id_prefix}linux" href="https://me.ryzom.com/?download=linux" target="_self" class="text-white effect-hover-color-link">
			<i class="fab fa-linux ml-2 mr-2"></i>
		</a>
	</span>

	<span>
		<a id="{$button_id_prefix}steam" href="https://store.steampowered.com/app/373720/Ryzom/" target="_blank" class="text-white effect-hover-color-link">
			<i class="fab fa-steam-square ml-2 mr-2"></i>
		</a>
	</span>
</div>

<div class="text-center">
	<a href="#" onclick="goToDownloadpage(); return false;">{t 'me~system_requirements'}</a>
</div>