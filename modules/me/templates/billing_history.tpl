<div id="history-{$id}" class="box-border" style="overflow-x: auto;">
	<table width="100%">
		<tr>
			<th align="center">{t 'date'}</th><th align="center">{t 'date_end'}</th><th align="center">{t 'provider'}</th><th align="center">{t 'timelenght'}</th><th align="center">{t 'amout'}</th><th align="center">{t 'status'}</th><th align="center">{t 'id'}</th><th>{t 'action'}</th>
		</tr>
		{if isset($infos['coupons'])}
		{assign $i=1}
		{foreach $infos['coupons'] as $coupon}
			{if $coupon[2] == "Freetime"}
				<tr class="row{$i % 2} lightgreen">
			{elseif $coupon[2] == "Suspension"}
				<tr class="row{$i % 2} red">
			{elseif $coupon[5] == "GIFT"}
				<tr class="row{$i % 2} yellow">
			{elseif $coupon[5] != "SUCCESS" && $coupon[5] != "CAPTURED" && $coupon[5] != "PAYMENT" && $coupon[5] != "AUTHORISED"}
				<tr class="row{$i % 2} grey">
			{else}
				<tr class="row{$i % 2}">
			{/if}
			{assign $i=$i+1}
			{foreach $coupon as $j => $c}
				{if $j < 2}
				<td align="center">{langdate $c}</td>
				{else}
				<td align="center">{$c}</td>
				{/if}
			{/foreach}
			</tr>
		{/foreach}
		{/if}
	</table>
</div>