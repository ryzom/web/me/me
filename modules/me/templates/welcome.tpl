{foreach $accounts as $id => $infos}
<div class="box-frame account" id="account_{$id}">
	{if !$infos['validated']}
		<div id="message-unvalidated-email">{t 'unvalidated_email'}<br />
			<a class="round" href="?ask_email_validation=1{if $show_user_id}&show_user={$show_user_id}{/if}">{t 'resend_email_validation'}</a>
		</div>
	{/if}

	{if $infos['available_voucher']}
		<div id="message-voucher">{$infos['available_voucher_infos']}<br />
			<a class="round" href="{$infos['available_voucher_url']}">
				<i class="fa fa-gift" aria-hidden="true"></i> {$infos['available_voucher']}
			</a>
		</div>
	{/if}

	<strong>{if $show_user_id}<a href="https://app.ryzom.com/app_admin/get_infos.php?type=uid&value={$infos['uid']}" target="_blank">{$infos['uid']}</a>{else}{$infos['uid']}{/if}</strong> - <span class="account-name purple" >{$infos['name']}</span> ({$infos['email']} {if $show_user_id}<a href="{jurl 'support~default:sendmail', array('show_user' => $show_user_id)}"><i class="fa-solid fa-envelope"></i>Send Email</a>{/if})
	{if $id != 0} 
		<span><a href="{jurl 'me~profile:index', ['uid' => $infos['uid']]}"><i class="fas fa-edit"></i></a></span>
		<span class="float-right"><a href="{jurl 'me~hideAccount', ['uid' => $infos['uid']]}"><i class="fas fa-window-close"></i></a></span>
	{/if}
	<div class="float-right">
		<div class="text-right"> 
		{if $infos['access_atys']} 
			<a href="#" id="atys-link" onclick="switchShard('atys'); return false;" class="shard-name font900 selected">Atys</a>
		{/if}
		{if $infos['access_yubo']}
			<a href="#" id="yubo-link" onclick="switchShard('yubo'); return false;" class="shard-name font900">Yubo</a>
			<a href="#" id="gingo-link" onclick="switchShard('gingo'); return false;" class="shard-name font900">Gingo</a>
		{/if}
				</div>
		{if $infos['access_atys']}
			<div id="atys" class="shard_chars">
			{foreach $infos['characters'] as $char}
				{if $char}
				<span class="char">{$char}</span>
				{/if}
			{/foreach}
			</div>
		{/if}

		{if $infos['access_yubo']}
			<div id="yubo" class="shard_chars hidden">
			{foreach $infos['characters_yubo'] as $char}
				{if $char}
				<span class="char">{$char}</span>
				{/if}
			{/foreach}
			</div>
			<div id="gingo" class="shard_chars hidden">
			{foreach $infos['characters_gingo'] as $char}
				{if $char}
				<span class="char">{$char}</span>
				{/if}
			{/foreach}
			</div>
		{/if}
	</div>
	<div id="subscription-frame">
		{if isset($infos['subscription']) && $infos['subscription']}
			<div class="me-subbox">
				{if isset($infos['subscription']['suspension']) && $infos['subscription']['suspension']}
					<div class="pink sub-status round-up"><strong>{t 'suspended_account'}</strong> - 
				{else}
					<div class="badge-secondary sub-status round-up"><strong>{t 'premium_account'}</strong> - 
				{/if}
				{if $infos['subscription']['type'] == 'sub'}
					{t 'subscribed_'.$infos['subscription']['months']} - {t 'next_billing_date'} {$infos['subscription']['end']} {if $infos['subscription']['type'] == 'oldsub'}<strong>(Worldpay)</strong>{/if}
				{else}
					{t 'end_in'} {$infos['subscription']['countdown']} ({$infos['subscription']['end']})
				{/if}
				{if $infos['subscription']['type'] == 'freeze'} - <strong>Permanant F2P</strong>{/if}
				</div>
				<div class="black round-bottom" style="background-color: #111">
					<div id="account_informations">
						{t 'account_creation_date'} {$infos['creation_date']}<br />
						{t 'account_last_connexion'} {$infos['last_connexion']}<br />
					</div>
					<div class="clear"></div>
					<a onclick="switchHidden('history-{$id}')" class="button float-right badge-secondary round"><i class="fas fa-history"></i> {t 'history'}</a>
					{if $infos['subscription']['type'] != 'freeze'}
					{if $infos['subscription']['type'] == 'sub'}
						<a href="{ryurl 'billing~payment:cancel'}" class="button round">{t 'cancel_your_subscription'}</a>
					{elseif $infos['subscription']['type'] == 'oldsub'}
						<a href="{ryurl 'billing~payment:cancel'}" class="unsub round">{t 'cancel_your_subscription'} <strong>(Worldpay)</strong></a>
					{/if}
					{if $infos['subscription']['type'] != 'sub' || $show_user_id}
						{if $show_user_id}
						<a onclick="switchHidden('{$id}')" class="button float-right badge-secondary round"><i class="fas fa-user-shield"></i> CSR Options</a>
						{else}
						<a onclick="switchHidden('{$id}')" class="button float-right badge-secondary round"><i class="fas fa-money-check-alt"></i> {t 'add_premium_time'}</a>
						{/if}
					{/if}
					<a class="float-right" href="/billing/donate"><img src="/images/donate.png" height="48px" /></a>
					{/if}
					<div class="clear"></div>
				</div>
				<div id="{$id}" class="hidden padding5">
					{$SUBSCRIPTIONS}
					{if $infos['subscription']['type'] == 'sub'}
						<a href="{ryurl 'billing~payment:cancel'}" class="cancel-sub">{t 'cancel_your_subscription'}</a>
					{elseif $infos['steamid']}
						<a href="{jurl 'billing~steam:unlink', ['show_user' => $show_user_id]}" class="cancel-sub">{t 'unlin_from_steam'}{if !$show_user_id} [{$infos['steamName']}]{/if}</a>
					{/if}
					<br />
					{if !$show_user_id}
						{ingroup '!','PRIVS'} 
							{include 'me~prices_full'}
						{/ingroup}
					{/if}
				</div>
			</div>
		{else}
			<div class="padding-5 me-subbox">
				<div id="account_informations">
					{t 'account_creation_date'} {$infos['creation_date']}<br />
					{t 'account_last_connexion'} {$infos['last_connexion']}<br />
				</div>
				<br />
				<br />
				{t 'free_with_restrictions'}
				<br />
				{if $id == 0}
				<br />
				<div>
					{$SUBSCRIPTIONS}
					{if $infos['steamid']}
						<a href="{jurl 'billing~steam:unlink', ['show_user' => $show_user_id]}" class="cancel-sub">{t 'unlin_from_steam'}{if !$show_user_id} [{$infos['steamName']}]{/if}</a>
					{/if}
				</div>
				{/if}
				{if !$show_user_id}
					{ingroup '!','PRIVS'} 
						{include 'me~prices_full'}
					{/ingroup}
				{/if}
			</div>
		{/if}
			
	</div>
	{include 'me~billing_history'}
	{allow account:logs}
		{include 'me~billing_logs'}
	{/allow}

	<div class="clear"></div>
</div>
{/foreach}


<script>
	enableToggleVisibility('expand');
</script>
