<div id="logs-{$id}" class="billing-logs box-border">
	<table width="100%">
		<tr>
			{foreach $infos['logs_fields'] as $k}
				<th align="center">{$k|upperfirst}</th>
			{/foreach}
		</tr>
		{if isset($infos['logs'])}
		{assign $i=1}
		{foreach $infos['logs'] as $log}
			<tr class="row{$i % 2}">
			{assign $i=$i+1}
			{foreach $infos['logs_fields'] as $j => $k}
				{if $j == 0}
					<td align="center">{langdate $log->$k}</td>
				{else}
					<td align="center">{$log->$k}</td>
				{/if}
			{/foreach}
			</tr>
		{/foreach}
		{/if}
	</table>
</div>