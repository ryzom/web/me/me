{meta_html title $title.' - Me'}
{if !$ig}
<div id="navigation">
	<div id="welcome">
		<span id="dots" onclick="switchDisplay('menu-content');"><i class="fa-solid fa-bars"></i></i></span>
		{if $username}
			{t 'me~welcome'} <strong>{$username}</strong>
		{else if isset($email)}
			{t 'me~welcome'} <strong>{$email}</strong>
		{/if}
		{if $ticket_id}
			<span class="float-right purple bold">Ticket {$ticket_id}</span>
			<div class="clear"></div>
		{/if}
	</div>
	<ul id="menu-content" class="nav flex-md-column">
		{if $username}
		<li class="nav-item {if $module == 'me'}selected{/if}">
			<a href="/{if $extra_params}?{$extra_params}{/if}" class="nav-link">
				<i class="fa-fw fas fa-dice-d6"></i>
				{if $show_user_id}
					<span class="nav-username" class="purple">{$show_user_name}</span>
				{else}
					{if $username != '#Guest'} {t 'me~ryzom_account'} {else} {t 'me~box_login_title_line_1'} {/if}
				{/if}
			</a>
		</li>
		{else}
		<li class="nav-item">
			<a href="/{if $extra_params}?{$extra_params}{/if}" class="nav-link">
				<i class="fa-fw fas fa-dice-d6"></i>{t 'me~box_login_title_line_1'}
			</a>
		</li>
		{/if}
		{if $username}
			{if $username != '#Guest'}
			<li class="nav-item {if $module == 'profile'}selected{/if}">
				<a href="/profile{if $extra_params}?{$extra_params}{/if}" class="nav-link">
					<i class="fa-fw fas fa-user-circle"></i>{t 'me~personal_data'}
				</a>
				{if $show_user_id}{allow account:show_profiles}<span class="nav-username" class="orange">{$show_user_name}</span>{/allow}{/if}
			</li>
			{/if}
		{/if}
		
		<li class="nav-item {if $module == 'support'}selected{/if}">
			<a href="/support/ticket/create/?{if $extra_params}{$extra_params}&{/if}{if isset($email) && $email}email={$email}&{/if}{if isset($token) && $token}token={$token}&{/if}" class="nav-link">
				<i class="fa-fw fas fa-ticket-alt"></i>{t 'me~support'}
			</a>
		</li> 
		{allow support:all}
			<li class="nav-sub-item{if isset($channel) && $channel->name == 'Search'} selected{/if}">
				<a href="/support/search" class="nav-sub-link">{i '16/zoom'}<strong><span> Search</span></strong></a>
			</li>
		{/allow}

		<li class="nav-sub-item{if isset($channel) && $channel->name == 'New Ticket'} selected{/if}">
			<a href="/support/ticket/create" class="nav-sub-link">{i '16/new'}<strong><span> {t 'support~new_ticket'}</span></strong></a>
		</li>
		
		{if isset($channels)}
		<li>
			<ul id="menu_support" class="nav">
				{include 'support~menu'}
			</ul>
		</li>
		{/if}

		
		{if $username}
			{allow translator:all}
				<li class="nav-item {if $module == 'translator'}selected{/if}">
					<a href="/translator" class="nav-link">
						<i class="fa-fw fas fa-flag"></i>{t 'me~translations'}
					</a>
				</li>
			{/allow}
			{allow marketing:all}
				<li class="nav-item {if $module == 'marketing'}selected{/if}">
					<a href="/marketing" class="nav-link">
						<i class="fa-fw fas fa-bullseye"></i>{t 'me~marketing'}
					</a>
				</li>
			{/allow}
			{allow admin:all}
				<li class="nav-item {if $module == 'admin'}selected{/if}">
					<a href="/admin" class="nav-link">
						<i class="fa-fw fas fa-user-shield"></i>{t 'me~admin'}
					</a>
				</li>
			{/allow}
			{if $username != '#Guest'}
			<li class="nav-item">
				<a href="#" onclick="return logout();" class="nav-link">
					<i class="fa-fw fas fa-sign-out-alt"></i>{t 'me~logout'}
				</a>
			</li>
			{/if}
		{/if}
	</ul>
</div>
{/if}