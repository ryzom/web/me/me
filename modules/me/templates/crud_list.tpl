{allow no_soup_for_you}
<h1>{@jelix~crud.title.list@}</h1>
<div class="big-frame">
<table class="records-list" width="100%">
<thead>
<tr>
	{foreach $properties as $propname}
	<th>
		{if $showPropertiesOrderLinks && array_key_exists($propname, $propertiesForListOrder)}<a href="{jurl '#~#', array($offsetParameterName=>$page, 'listorder'=>$propname)}"
											class="view-order{if isset($sessionForListOrder[$propname])} {if $sessionForListOrder[$propname] == 'asc'} order-asc{elseif $sessionForListOrder[$propname] == 'desc'} order-desc{/if}{/if}">{/if}
		{if isset($controls[$propname])}{$controls[$propname]->label|eschtml}{else}{$propname|eschtml}{/if}
		{if $showPropertiesOrderLinks && array_key_exists($propname, $propertiesForListOrder)}</a>{/if}
	</th>
	{/foreach}
	<th>&nbsp;</th>
</tr>
</thead>
<tbody>
{foreach $list as $record}
<tr class="{cycle array('odd','even')}">
	{foreach $properties as $propname}
	<td align="center">{$record->$propname|eschtml}</td>
	{/foreach}
	<td align="center">
		{if isset($extraActions)}
		{foreach $extraActions as $extraAction}
			<a href="{ryurl $extraAction['Action'], ['id'=>$record->$primarykey, $offsetParameterName=>$page]}">{$extraAction['Label']}</a>
		{/foreach}
		{/if}
		{if isset($editable)}
			<a href="{ryurl 'groups:preupdate', ['id'=>$record->$primarykey, $offsetParameterName=>$page]}"><i class="fas fa-edit"></i></a>
		{/if}
		{if isset($deletable)}
	<td align="center">
			<a href="{ryurl 'groups:delete', ['id'=>$record->$primarykey, $offsetParameterName=>$page]}" onclick="return confirm('{@jelix~crud.confirm.deletion@}')"><i class="red fas fa-trash"></i></a>
	</td>
		{/if}
	</td>
</tr>
{/foreach}
</tbody>
</table>
{if $recordCount > $listPageSize}
<p class="record-pages-list">{@jelix~crud.title.pages@} : {pagelinks $listAction, array(),  $recordCount, $page, $listPageSize, $offsetParameterName }</p>
{/if}
<p><a href="{ryurl $createAction}" class="crud-link">{@jelix~crud.link.create.record@}</a>.</p>

</div>
{/allow}