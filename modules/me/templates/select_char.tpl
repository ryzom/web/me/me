
<div class="container slim pt-5 pb-5 text-center">
	<h1>Please Select Your Char</h1>

	<p>
	{$MAIN}
	</p>

	<h2 class="mt-4 mb-4">OR</h2>

	<a href="{$logout_url}" class="btn btn-secondary font-large" style="margin: 5px"><i class="fas fa-sign-out-alt"></i> LOGOUT</a>

</div>
