<div class="messages-box">{if isset($messages) && $messages}
    <div class="alert alert-info" style="background-color: {$info_color}">{$messages}</div>
{elseif  isset($warnings) && $warnings}
    <div class="alert alert-warning" style="background-color: {$warning_color}">{$warnings}</div>
{elseif  isset($errors) && $errors}
    <div class="alert alert-danger" style="background-color: {$danger_color}">{$errors}</div>
{/if}</div>