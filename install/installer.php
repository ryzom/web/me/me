<?php
/**
* @package   me
* @author    Ulukyn
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

require_once (__DIR__.'/../application.init.php');

\Jelix\Scripts\Installer::launch();
