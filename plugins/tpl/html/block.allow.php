<?php

function jtpl_block_html_allow($compiler, $begin, $params=[])
{
	if ($begin) {
		$content = '';
		if (count($params) && strpos($params[0], ':') !== false)
			$page = array_shift($params);
		else {
			$page = str_replace('index.php/', '', $_SERVER['REQUEST_URI']);
			$pos = strpos($page, '?');
			if ($pos === false)
				$page = substr($page, 1);
			else
				$page = substr($page, 1, $pos-1);
			$spage = explode('/', $page);
			$page = $spage[0];
			if (isset($spage[1]))
				$page .= ':'.$spage[1];
			if (isset($spage[2]))
				$page .= '-'.$spage[2];
		}
		$user = Ry\Account::spawn();
		if (!$user->checkAccess($page)) {
			if (count($params))
				$content = 'echo "'._t(trim($params[0])).'";'."\n";
			$content .= ' if (false) {';
		} else
			$content = ' if (true) {';
		
	} else {
		$content = ' } ';
	}
	
	return $content;
}