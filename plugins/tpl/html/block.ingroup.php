<?php

function jtpl_block_html_ingroup($compiler, $begin, $params=[])
{
	if ($begin) {
		$content = '';
		$user = Ry\Account::spawn();
		if ($params[0] == '\'!\'') {
			if ($user->inGroup(substr($params[1], 1, strlen($params[1])-2)))
				$content .= ' if (false) {';
			else
				$content = ' if (true) {';
		} else {
			if ($user->inGroup(substr($params[0], 1, strlen($params[0])-2)))
				$content .= ' if (false) {';
			else
				$content = ' if (true) {';
		}
	} else {
		$content = '}';
	}
	
	return $content;
}