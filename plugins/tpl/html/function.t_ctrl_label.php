<?php

 
function jtpl_function_html_t_ctrl_label($tpl, $ctrlname = '', $format = '')
{
    if ((!isset($tpl->_privateVars['__ctrlref']) || $tpl->_privateVars['__ctrlref'] == '') && $ctrlname == '') {
        return;
    }

    if ($ctrlname == '') {
        $ctrl = $tpl->_privateVars['__ctrl'];
    } else {
        $ctrls = $tpl->_privateVars['__form']->getControls();
        if (!isset($ctrls[$ctrlname])) {
            throw new jException(
                'jelix~formserr.unknown.control',
                array($ctrlname, $tpl->_privateVars['__form']->getSelector(), $tpl->_templateName)
            );
        }
        $ctrl = $ctrls[$ctrlname];
    }
    if ($ctrl->type == 'hidden') {
        return;
    }

    if (!$tpl->_privateVars['__form']->isActivated($ctrl->ref)) {
        return;
    }

    $editMode = !(isset($tpl->_privateVars['__formViewMode']) && $tpl->_privateVars['__formViewMode']);

    if ($editMode) {
        if ($ctrl->type == 'submit' || $ctrl->type == 'reset') {
            return;
        }
    } elseif ($ctrl->type == 'captcha') {
        return;
    }
    
    if ($ctrl->type == 'group') {
        foreach( $ctrl->getChildControls() as $child) {
            $child->label = Ry\Common::t($child->label);
            if ($child->help)
                $child->help = Ry\Common::t($child->help);
        }
    }

    $ctrl->label =  Ry\Common::t($ctrl->label);
    $tpl->_privateVars['__formbuilder']->outputControlLabel($ctrl, $format, $editMode);
}
