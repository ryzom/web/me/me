<?php

function jtpl_function_html_request($tpl, $name)
{
	$infos = [];
	$infos['module'] = \jApp::coord()->request->params['module'];
	$infos['Module'] = ucfirst($infos['module']);
	$infos['full_action'] = \jApp::coord()->request->params['action'];
	$ctrl = explode(':', $infos['full_action']);
	$infos['controller'] = $ctrl[0];
	$infos['action'] = $ctrl[1];
	$infos['Controller'] = ucfirst($ctrl[0]);
	$infos['Action'] = ucfirst($ctrl[1]);
	
	if (isset($infos[$name]))
		echo $infos[$name];
	
}
