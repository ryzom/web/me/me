<?php
/**
 * wikirenderer3 (markdown) syntax to xhtml
 *
 * @package WikiRenderer
 * @subpackage rules
 * @author Laurent Jouanneau
 * @copyright 2003-2006 Laurent Jouanneau
 * @link http://wikirenderer.jelix.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public 2.1
 * License as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 *
 */


class markdown_to_text extends WikiRendererConfig{

	public $defaultTextLineContainer = 'WikiTextLine';

	public $textLineContainers = array('WikiTextLine' => array( 'markdownxtext_color_open', 'markdownxtext_color_close', 'markdownxtext_strong','markdownxtext_em','markdownxtext_q',
	'markdownxtext_cite','markdownxtext_acronym','markdownxtext_link','markdownxtext_image','markdownxtext_anchor'));

	/**
	* liste des balises de type bloc reconnus par WikiRenderer.
	*/
	public $bloctags = array('markdownxtext_title','markdownxtext_list','markdownxtext_pre', 'markdownxtext_code', 'markdownxtext_hr',
						'markdownxtext_blockquote','markdownxtext_definition','markdownxtext_table');


	public $simpletags = array();

	public function processLink($url, $tagName='') {
		$label = $url;
		if(strlen($label) > 40)
			$label = substr($label,0,40).'(..)';

		if(strpos($url,'javascript:') !== false) // for security reason
			$url = '#';
		return array($url, $label);
	}
	
	public function onStart($text){
		return str_replace(["\r", "\n"], ["", "\n"], $text);
	}
	
}

// =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = déclarations des tags inlines

class markdownxtext_strong extends WikiTagXhtml {
	protected $name = 'strong';
	public $beginTag = '**';
	public $endTag = '**';
	
	public function getContent() {
		return $this->_doEscape($this->wikiContentArr[0]);
	}
}

class markdownxtext_em extends WikiTagXhtml {
	protected $name = 'em';
	public $beginTag = '__';
	public $endTag = '__';
	public function getContent() {
		return $this->_doEscape($this->wikiContentArr[0]);
	}
}

//// ?????
class markdownxtext_q extends WikiTagXhtml {
	protected $name = 'q';
	public $beginTag = '^^';
	public $endTag = '^^';
	protected $attribute = array('$$','lang','cite');
	public $separators = array('|');
	
	public function getContent() {
		return $this->_doEscape($this->wikiContentArr[0]);
	}
}

//// ?????
class markdownxtext_cite extends WikiTagXhtml {
	protected $name = 'cite';
	public $beginTag = '{{';
	public $endTag = '}}';
	protected $attribute = array('$$','title');
	public $separators = array('|');
	
	public function getContent() {
		return $this->_doEscape($this->wikiContentArr[0]);
	}
	
}

//// ?????
class markdownxtext_acronym extends WikiTagXhtml {
	protected $name = 'acronym';
	public $beginTag = '??';
	public $endTag = '??';
	protected $attribute = array('$$','title');
	public $separators = array('|');
	
	public function getContent() {
		return $this->_doEscape($this->wikiContentArr[0]);
	}
	
}

//// ?????
class markdownxtext_anchor extends WikiTagXhtml {
	protected $name = 'anchor';
	public $beginTag = '~~';
	public $endTag = '~~';
	protected $attribute = array('name');
	public $separators = array('|');
	public function getContent(){
		return $this->_doEscape($this->wikiContentArr[0]);
	}
}


class markdownxtext_link extends WikiTagXhtml {
	protected $name = 'a';
	public $beginTag = '[';
	public $endTag = ')';
	
	
	public function getContent(){
		$link = $this->wikiContentArr[0];
		$slink = explode('](', $link);
		if (count($slink) == 2) {
			return $this->_doEscape($text);
		}
		return $this->_doEscape($link);
	}

	public function isOtherTagAllowed() {
		return false;
	}
}

class markdownxtext_image extends WikiTagXhtml {
	protected $name = 'img';
	public $beginTag = '![';
	public $endTag = ')';
	
	
	public function getContent(){
		$link = $this->wikiContentArr[0];
		$slink = explode('](', $link);
		if (count($slink) == 2) {
			$surl = explode(' ', $slink[1], 2);
			$params = 'src="'.$this->_doEscape($surl[0]).'"';
			$params .= ' alt="'.$this->_doEscape($slink[0]).'"';
			if (count($surl) == 2)
				$params .= ' title="'.$this->_doEscape($surl[1]).'"';
			return '<img '.$params.'/>';
		}
		return $this->_doEscape($link);
	}

	public function isOtherTagAllowed() {
		return false;
	}
}


class markdownxtext_color_open extends WikiTagXhtml {
	protected $name = 'color_open';
	public $beginTag = '[color=';
	public $endTag = ']';
	
	
	public function getContent(){
		return '';
	}

	public function isOtherTagAllowed() {
		return false;
	}
}

class markdownxtext_color_close extends WikiTagXhtml {
	protected $name = 'color_close';
	public $beginTag = '[/color';
	public $endTag = ']';
	
	
	public function getContent(){
		return '';
	}

	public function isOtherTagAllowed() {
		return false;
	}
}

// =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = déclaration des différents bloc wiki

/**
 * traite les signes de types liste
 */
class markdownxtext_list extends WikiRendererBloc {

	public $type = 'list';
	protected $_previousTag;
	protected $_firstItem;
	protected $_firstTagLen;
	protected $regexp = '/^([ *-]+) (.*)/';

	public function getRenderedLine() {
		return $this->_renderInlineTag($this->_detectMatch[2]);
	}
}


/**
 * traite les signes de types table
 */
class markdownxtext_table extends WikiRendererBloc {
	public $type = 'table';
	protected $regexp = "/^\s*\| ?(.*)/";
	protected $_openTag = '<table border = "1">';
	protected $_closeTag = '</table>';

	protected $_colcount = 0;

	public function open(){
		$this->_colcount = 0;
		return $this->_openTag;
	}


	public function getRenderedLine(){
		$result = explode(' | ',trim($this->_detectMatch[1]));
		$str = '';
		$t = '';

		if((count($result) != $this->_colcount) && ($this->_colcount != 0))
			$t = '</table><table border = "1">';
		$this->_colcount = count($result);

		for($i = 0; $i < $this->_colcount; $i++){
			$str .= '<td>'. $this->_renderInlineTag($result[$i]).'</td>';
		}
		$str = $t.'<tr>'.$str.'</tr>';

		return $str;
	}

}

/**
 * traite les signes de types hr
 */
class markdownxtext_hr extends WikiRendererBloc {

	public $type = 'hr';
	protected $regexp = '/^\>-*\</';
	protected $_closeNow = true;

	public function getRenderedLine(){
		return '<hr />';
	}

}

/**
 * traite les signes de types titre
 */
class markdownxtext_title extends WikiRendererBloc {
	public $type = 'title';
	protected $regexp = '/^(#+) (.*)/';
	protected $_closeNow = true;
	protected $_order = true;

	public function getRenderedLine(){
		return $this->_renderInlineTag($this->_detectMatch[2]);
	}
}


class markdownxtext_code extends WikiRendererBloc {
	public $type = 'code';
	static protected $_end = true;
	protected $_text = '';

	public function detect($string){
		if ($string == '') {
			$this->_text = $string;
			return true;
		}
		if ($string[0] == "\t")
			$string = '```'.substr($string, 1).'```';
		$s = explode('```', ' '.$string);
		if (count($s) > 1) {
			$final = array_shift($s);
			foreach($s as $i => $line)
					$final .= $line;
			$this->_text = substr($final, 1);
			return true;
		}
		if (self::$_end)
			return false;

		$this->_text = $string;
		return true;
	}
	
	public function getRenderedLine() {
		return $this->_text;
	}
	
}

/**
 * traite les signes de types pre (pour afficher du code..)
 */
class markdownxtext_pre extends WikiRendererBloc {

	public $type = 'pre';
	protected $_openTag = '<pre>';
	protected $_closeTag = '</pre>';
	protected $isOpen = false;


	public function open(){
		$this->isOpen = true;
		return $this->_openTag;
	}

	public function close(){
		$this->isOpen = false;
		return $this->_closeTag;
	}

	public function getRenderedLine(){
		return htmlspecialchars($this->_detectMatch, ENT_COMPAT, $this->engine->getConfig()->charset);
	}

	public function detect($string){
		if($this->isOpen){
			if(preg_match('/(.*)<\/code>\s*$/',$string,$m)){
				$this->_detectMatch = $m[1];
				$this->isOpen = false;
			}else{
				$this->_detectMatch = $string;
			}
			return true;

		}else{
			if(preg_match('/^\s*<code>(.*)/',$string,$m)){
				if(preg_match('/(.*)<\/code>\s*$/',$m[1],$m2)){
					$this->_closeNow = true;
					$this->_detectMatch = $m2[1];
				}
				else {
					$this->_closeNow = false;
					$this->_detectMatch = $m[1];
				}
				return true;
			}else{
				return false;
			}
		}
	}
}


/**
 * traite les signes de type blockquote
 */
class markdownxtext_blockquote extends WikiRendererBloc {
	public $type = 'cite';
	protected $_closeNow = false;
	protected $_openTag = '<cite>';
	protected $_closeTag = '</cite>';
	protected $_text = '';

	public function detect($string){
		if (strlen($string) > 2 && $string[0] == '>' && $string[1] == ' ') {
			$this->_text = substr($string, 2);
			return true;
		}
		return false;
	}
	
	public function getRenderedLine() {
		return $this->_text;
	}
}

/**
 * traite les signes de type définitions
 */
class markdownxtext_definition extends WikiRendererBloc {

	public $type = 'dfn';
	protected $regexp = "/^\s*;(.*) : (.*)/i";
	protected $_openTag = '<dl>';
	protected $_closeTag = '</dl>';
	protected $_closeNow = true;

	public function getRenderedLine(){
		$dt = $this->_renderInlineTag($this->_detectMatch[1]);
		$dd = $this->_renderInlineTag($this->_detectMatch[2]);
		return "<dt>$dt</dt>\n<dd>$dd</dd>\n";
	}
}
