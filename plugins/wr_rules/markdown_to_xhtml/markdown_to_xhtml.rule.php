<?php
/**
 * wikirenderer3 (markdown) syntax to xhtml
 *
 * @package WikiRenderer
 * @subpackage rules
 * @author Laurent Jouanneau
 * @copyright 2003-2006 Laurent Jouanneau
 * @link http://wikirenderer.jelix.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public 2.1
 * License as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 *
 */


class markdown_to_xhtml extends WikiRendererConfig{

	public $defaultTextLineContainer = 'WikiTextLine';

	public $textLineContainers = array('WikiTextLine' => array( 'markdownxhtml_color_open', 'markdownxhtml_color_close', 'markdownxhtml_strong','markdownxhtml_em','markdownxhtml_q',
	'markdownxhtml_cite','markdownxhtml_acronym','markdownxhtml_link','markdownxhtml_image','markdownxhtml_anchor'));

	/**
	* liste des balises de type bloc reconnus par WikiRenderer.
	*/
	public $bloctags = array('markdownxhtml_title','markdownxhtml_title2','markdownxhtml_list','markdownxhtml_pre', 'markdownxhtml_code', 'markdownxhtml_hr',
						'markdownxhtml_blockquote','markdownxhtml_definition','markdownxhtml_table');


	public $simpletags = array();

	public function processLink($url, $tagName='') {
		$label = $url;
		if(strlen($label) > 40)
			$label = substr($label,0,40).'(..)';

		if(strpos($url,'javascript:') !== false) // for security reason
			$url = '#';
		return array($url, $label);
	}
	
	public function onStart($text){
		return str_replace(["\r", "\n"], ["", "\n"], $text);
	}
	
}

// =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = déclarations des tags inlines

class markdownxhtml_strong extends WikiTagXhtml {
	protected $name = 'strong';
	public $beginTag = '**';
	public $endTag = '**';
}

class markdownxhtml_em extends WikiTagXhtml {
	protected $name = 'em';
	public $beginTag = '__';
	public $endTag = '__';
}

//// ?????
class markdownxhtml_q extends WikiTagXhtml {
	protected $name = 'q';
	public $beginTag = '^^';
	public $endTag = '^^';
	protected $attribute = array('$$','lang','cite');
	public $separators = array('|');
}

//// ?????
class markdownxhtml_cite extends WikiTagXhtml {
	protected $name = 'cite';
	public $beginTag = '{{';
	public $endTag = '}}';
	protected $attribute = array('$$','title');
	public $separators = array('|');
}

//// ?????
class markdownxhtml_acronym extends WikiTagXhtml {
	protected $name = 'acronym';
	public $beginTag = '??';
	public $endTag = '??';
	protected $attribute = array('$$','title');
	public $separators = array('|');
}

//// ?????
class markdownxhtml_anchor extends WikiTagXhtml {
	protected $name = 'anchor';
	public $beginTag = '~~';
	public $endTag = '~~';
	protected $attribute = array('name');
	public $separators = array('|');
	public function getContent(){
		return '<a name="'.$this->_doEscape($this->wikiContentArr[0]).'"></a>';
	}
}


class markdownxhtml_link extends WikiTagXhtml {
	protected $name = 'a';
	public $beginTag = '[';
	public $endTag = ')';
	
	
	public function getContent(){
		$link = $this->wikiContentArr[0];
		$slink = explode('](', $link);
		if (count($slink) == 2) {
			$params = '';
			$text = $slink[0];

			$sslink = explode('#', $slink[1]);
			if (count($sslink) == 3 && $sslink[1] = 'copy') {
				$params .= ' onclick="navigator.clipboard.writeText(\''.str_replace(['\'', '"'], '', $sslink[2]).'\'); return false;"';
				$slink[1] = '#';
			} elseif ($slink[1][0] == '#') {
				$params .= ' target="_blank"';
				$slink[1] = substr($slink[1], 1);
			}

			
			$surl = explode(' ', $slink[1], 2);
			list($url, $label) = $this->config->processLink($surl[0]);
			$params .= 'href="'.$this->_doEscape($url).'"';
			if (count($surl) == 2)
				$params .= ' title="'.$this->_doEscape($surl[1]).'"';
			else
				$params .= ' title="'.$this->_doEscape($label).'"';
			return '<a '.$params.'>'.$this->_doEscape($text).'</a>';
		}
		return $this->_doEscape($link);
	}

	public function isOtherTagAllowed() {
		return false;
	}
}

class markdownxhtml_image extends WikiTagXhtml {
	protected $name = 'img';
	public $beginTag = '![';
	public $endTag = ')';
	
	
	public function getContent(){
		$link = $this->wikiContentArr[0];
		$slink = explode('](', $link);
		if (count($slink) == 2) {
			$surl = explode(' ', $slink[1], 2);
			$params = 'src="'.$this->_doEscape($surl[0]).'"';
			$params .= ' alt="'.$this->_doEscape($slink[0]).'"';
			if (count($surl) == 2)
				$params .= ' title="'.$this->_doEscape($surl[1]).'"';
			return '<img '.$params.'/>';
		}
		return $this->_doEscape($link);
	}

	public function isOtherTagAllowed() {
		return false;
	}
}


class markdownxhtml_color_open extends WikiTagXhtml {
	protected $name = 'color_open';
	public $beginTag = '[color=';
	public $endTag = ']';
	
	
	public function getContent(){
		$color = $this->wikiContentArr[0];
		return '<span style="color:'.$color.'">';
	}

	public function isOtherTagAllowed() {
		return false;
	}
}

class markdownxhtml_color_close extends WikiTagXhtml {
	protected $name = 'color_close';
	public $beginTag = '[/color';
	public $endTag = ']';
	
	
	public function getContent(){
		return '</span>';
	}

	public function isOtherTagAllowed() {
		return false;
	}
}

// =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = déclaration des différents bloc wiki

/**
 * traite les signes de types liste
 */
class markdownxhtml_list extends WikiRendererBloc {

	public $type = 'list';
	protected $_previousTag;
	protected $_firstItem;
	protected $_firstTagLen;
	protected $regexp = '/^([ *-]+) (.*)/';

	public function open() {
		$this->_previousTag = $this->_detectMatch[1];
		$this->_firstTagLen = strlen($this->_previousTag);
		$this->_firstItem = true;
		$t = $this->_previousTag;
		$str = '';
		for ($i = 0; $i < $this->_firstTagLen; $i++) {
			$str .= $t[$i] == '-'?"<ol>":"<ul>";
		}
		return $str;
	}
	
	public function close() {
		$t = $this->_previousTag;
		$str = '';

		for ($i = strlen($t); $i >= $this->_firstTagLen; $i--) {
			$str .= ($t[$i-1] == '-'?"</li></ol>":"</li></ul>");
		}
		return $str;
	}

	public function getRenderedLine() {
		$t = $this->_previousTag;
		$d = strlen($t) - strlen($this->_detectMatch[1]);
		$str = '';

		if( $d > 0 ) { // on remonte d'un ou plusieurs cran dans la hierarchie...
			$l = strlen($this->_detectMatch[1]);
			for($i = strlen($t); $i>$l; $i--){
				$str .= ($t[$i-1] == '-'?"</li></ol>":"</li></ul>");
			}
			$str .= "</li><li>";
			$this->_previousTag = substr($this->_previousTag,0,-$d); // pour étre sur...

		} elseif ( $d < 0 ) { // un niveau de plus
			$c = substr($this->_detectMatch[1],-1,1);
			$this->_previousTag .= $c;
			$str = ($c == '-'?"<ol><li>":"<ul><li>");

		} else {
			$str = ($this->_firstItem ? "<li>":"</li><li>");
		}
		$this->_firstItem = false;
		return $str.$this->_renderInlineTag($this->_detectMatch[2]);
	}
}


/**
 * traite les signes de types table
 */
class markdownxhtml_table extends WikiRendererBloc {
	public $type = 'table';
	protected $regexp = "/^\s*\| ?(.*)/";
	protected $_openTag = '<table border = "1">';
	protected $_closeTag = '</table>';

	protected $_colcount = 0;

	public function open(){
		$this->_colcount = 0;
		return $this->_openTag;
	}


	public function getRenderedLine(){
		$result = explode(' | ',trim($this->_detectMatch[1]));
		$str = '';
		$t = '';

		if((count($result) != $this->_colcount) && ($this->_colcount != 0))
			$t = '</table><table border = "1">';
		$this->_colcount = count($result);

		for($i = 0; $i < $this->_colcount; $i++){
			$str .= '<td>'. $this->_renderInlineTag($result[$i]).'</td>';
		}
		$str = $t.'<tr>'.$str.'</tr>';

		return $str;
	}

}

/**
 * traite les signes de types hr
 */
class markdownxhtml_hr extends WikiRendererBloc {

	public $type = 'hr';
	protected $regexp = '/^\>-*\</';
	protected $_closeNow = true;

	public function getRenderedLine(){
		return '<hr />';
	}

}

/**
 * traite les signes de types titre
 */
class markdownxhtml_title extends WikiRendererBloc {
	public $type = 'title';
	protected $regexp = '/^(#+) (.*)/';
	protected $_closeNow = true;

	protected $_minlevel = 3;
	protected $_maxlevel = 5;
	/**
	* indique le sens dans lequel il faut interpreter le nombre de signe de titre
	* true -> ! = titre , !! = sous titre, !!! = sous-sous-titre
	* false-> !!! = titre , !! = sous titre, ! = sous-sous-titre
	*/
	protected $_order = true;

	public function getRenderedLine(){
		$hx = $this->_minlevel + strlen($this->_detectMatch[1])-1;
		$hx = min($hx, $this->_maxlevel);
	return '<h'.$hx.'><strong>'.$this->_renderInlineTag($this->_detectMatch[2]).'</strong></h'.$hx.'>';
	}
}

class markdownxhtml_title2 extends WikiRendererBloc {
	public $type = 'title';
	protected $regexp = '/^(!+) (.*)/';
	protected $_closeNow = true;

	protected $_minlevel = 1;
	protected $_maxlevel = 6;
	/**
	* indique le sens dans lequel il faut interpreter le nombre de signe de titre
	* true -> ! = titre , !! = sous titre, !!! = sous-sous-titre
	* false-> !!! = titre , !! = sous titre, ! = sous-sous-titre
	*/
	protected $_order = true;

	public function getRenderedLine(){
		$hx = $this->_minlevel + strlen($this->_detectMatch[1])-1;
		$hx = min($hx, $this->_maxlevel);
	return '<h'.$hx.'>'.$this->_renderInlineTag($this->_detectMatch[2]).'</h'.$hx.'>';
	}
}



class markdownxhtml_code extends WikiRendererBloc {
	public $type = 'code';
	static protected $_end = true;
	protected $_text = '';

	public function detect($string){
		if ($string == '') {
			$this->_text = '';
			return true;
		}
		if ($string[0] == "\t")
			$string = '```'.substr($string, 1).'```';
		$s = explode('```', ' '.$string);
		if (count($s) > 1) {
			$final = array_shift($s);
			foreach($s as $i => $line) {
				if (self::$_end) {
					$final .= '<code>'.$line;
					self::$_end = false;
				} else {
					$final .= '</code>'.$line;
					self::$_end = true;
				}
			}
			$this->_text = substr($final, 1);
			return true;
		}
		if (self::$_end)
			return false;

		$this->_text = $string;
		return true;
	}
	
	public function getRenderedLine() {
		return $this->_text;
	}
	
}

/**
 * traite les signes de types pre (pour afficher du code..)
 */
class markdownxhtml_pre extends WikiRendererBloc {

	public $type = 'pre';
	protected $_openTag = '<pre>';
	protected $_closeTag = '</pre>';
	protected $isOpen = false;


	public function open(){
		$this->isOpen = true;
		return $this->_openTag;
	}

	public function close(){
		$this->isOpen = false;
		return $this->_closeTag;
	}

	public function getRenderedLine(){
		return htmlspecialchars($this->_detectMatch, ENT_COMPAT, $this->engine->getConfig()->charset);
	}

	public function detect($string){
		if($this->isOpen){
			if(preg_match('/(.*)<\/code>\s*$/',$string,$m)){
				$this->_detectMatch = $m[1];
				$this->isOpen = false;
			}else{
				$this->_detectMatch = $string;
			}
			return true;

		}else{
			if(preg_match('/^\s*<code>(.*)/',$string,$m)){
				if(preg_match('/(.*)<\/code>\s*$/',$m[1],$m2)){
					$this->_closeNow = true;
					$this->_detectMatch = $m2[1];
				}
				else {
					$this->_closeNow = false;
					$this->_detectMatch = $m[1];
				}
				return true;
			}else{
				return false;
			}
		}
	}
}


/**
 * traite les signes de type blockquote
 */
class markdownxhtml_blockquote extends WikiRendererBloc {
	public $type = 'cite';
	protected $_closeNow = false;
	protected $_openTag = '<cite>';
	protected $_closeTag = '</cite>';
	protected $_text = '';

	public function detect($string){
		if (strlen($string) > 2 && $string[0] == '>' && $string[1] == ' ') {
			$this->_text = substr($string, 2);
			return true;
		}
		return false;
	}
	
	public function getRenderedLine() {
		return $this->_text;
	}
}

/**
 * traite les signes de type définitions
 */
class markdownxhtml_definition extends WikiRendererBloc {

	public $type = 'dfn';
	protected $regexp = "/^\s*;(.*) : (.*)/i";
	protected $_openTag = '<dl>';
	protected $_closeTag = '</dl>';
	protected $_closeNow = true;

	public function getRenderedLine(){
		$dt = $this->_renderInlineTag($this->_detectMatch[1]);
		$dd = $this->_renderInlineTag($this->_detectMatch[2]);
		return "<dt>$dt</dt>\n<dd>$dd</dd>\n";
	}
}
