// -------------------------------------------------------------------
// markItUp!
// -------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// -------------------------------------------------------------------
// MarkDown tags example
// http://en.wikipedia.org/wiki/Markdown
// http://daringfireball.net/projects/markdown/
// -------------------------------------------------------------------
// Feel free to add more tags
// -------------------------------------------------------------------

function setCaretPosition(elem, caretPos) {
	if(elem.createTextRange) {
		var range = elem.createTextRange();
		range.move('character', caretPos);
		range.select();
	}
	else {
		if(elem.selectionStart) {
			elem.focus();
			elem.setSelectionRange(caretPos, caretPos);
		}
		else
			elem.focus();
	}
}

function addAtBegin(markItUp, text) {
		markItUp.textarea.value = markItUp.textarea.value.replace("\r", "");
		var stext = markItUp.textarea.value.split("\n");
		var i = 0;
		var c = stext[0].length+1;
		console.log(stext)
		console.log(markItUp.caretPosition+" - "+c)
		while (markItUp.caretPosition >= c) {
			if (i < stext.length-1) {
				i++;
				console.log(stext[i], stext[i].length+1, c)
				c = c + stext[i].length+1;
			} else {
				c = markItUp.caretPosition+1;
			}
			console.log(c)
		}
		
		if (stext[i][0] != text && stext[i][0] != " ") {
			stext[i] = text+" "+stext[i];
			markItUp.caretPosition = markItUp.caretPosition+2;
		} else {
			stext[i] = text+stext[i];
			markItUp.caretPosition++;
		}
		markItUp.textarea.value = stext.join("\n");
		setCaretPosition(markItUp.textarea, markItUp.caretPosition)
		return;
	}

markitup_markdown_settings = {
	previewParser: function(content) {
		window.scrollTo(0, document.body.scrollHeight);
		return content;
	}, 
	 previewHandler:         true,
	  resizeHandle:           true,
	//previewInWindow: 'width=800, height=600, resizable=yes, scrollbars=yes',
	previewParserPath:'/api/markdown/preview',
	onShiftEnter: {keepDefault:false, openWith:'\n\n'},
	markupSet: [
		{name:'Heading 1', key:'1', openWith: function(markItUp){addAtBegin(markItUp, '#')}},
		{name:'Heading 2', key:'2', openWith: function(markItUp){addAtBegin(markItUp, '##')}},
		{name:'Heading 3', key:'3', openWith: function(markItUp){addAtBegin(markItUp, '###')}},
		//{separator:'---------------' },
		{name:'Bold', key:'B', openWith:'**', closeWith:'**'},
		{name:'Italic', key:'I', openWith:'_', closeWith:'_'},
		//{separator:'---------------' },
		{name:'Bulleted List', openWith: function(markItUp){addAtBegin(markItUp, '*')}},
		{name:'Numeric List', openWith: function(markItUp){addAtBegin(markItUp, '-')}},
		//{separator:'---------------' },
		/*{name:'Picture', key:'P', replaceWith:'![[![Alternative text]!]]([![Url:!:http://]!] "[![Title]!]")'},
		{name:'Link', key:'L', openWith:'[', closeWith:']([![Url:!:http://]!] "[![Title]!]")', placeHolder:'Your text to link here...' },
		{separator:'---------------'},	
		{name:'Quotes', openWith:'> ', multiline:true},*/
		{name:'Code Block / Code', openWith:'```', closeWith:'```'},
		{separator:'---------------'},
		{name:'Preview', call:'preview', className:'preview'}
	]
}

// mIu nameSpace to avoid conflict.
miu = {
	markdownTitle: function(markItUp, char) {
		heading = '';
		n = $.trim(markItUp.selection||markItUp.placeHolder).length;
		for(i = 0; i < n; i++) {
			heading += char;
		}
		return '\n'+heading;
	}
}
