tarteaucitron.init({
  "privacyUrl": "https://ryzom.com/privacy", /* Privacy policy url */
  "bodyPosition": "top", /* or top to bring it as first element for accessibility */

  "hashtag": "#ryzom", /* Open the panel with this hashtag */
  "cookieName": "ryzom-cookies", /* Cookie name */

  "orientation": "middle", /* Banner position (top - bottom) */

  "groupServices": false, /* Group services by category */
  "serviceDefaultState": "wait", /* Default state (true - wait - false) */
				   
  "showAlertSmall": false, /* Show the small banner on bottom right */
  "cookieslist": false, /* Show the cookie list */
				   
  "closePopup": true, /* Show a close X on the banner */

  "showIcon": true, /* Show cookie icon to manage cookies */
  "iconSrc": "images/cookie-settings.svg", /* Optionnal: URL or base64 encoded image */
  "iconPosition": "BottomLeft", /* BottomRight, BottomLeft, TopRight and TopLeft */

  "adblocker": false, /* Show a Warning if an adblocker is detected */
			    
  "DenyAllCta" : false , /* Show the deny all button */
  "AcceptAllCta" : true, /* Show the accept all button when highPrivacy on */
  "highPrivacy": true, /* HIGHLY RECOMMANDED Disable auto consent */
				   
  "handleBrowserDNTRequest": true, /* If Do Not Track == 1, disallow all */

  "removeCredit": true, /* Remove credit link */
  "moreInfoLink": true, /* Show more info link */

  "useExternalCss": false, /* If false, the tarteaucitron.css file will be loaded */
  "useExternalJs": false, /* If false, the tarteaucitron.js file will be loaded */

  "cookieDomain": ".ryzom.com", /* Shared cookie for multisite */
				  
  "readmoreLink": "", /* Change the default readmore link */

  "mandatory": true, /* Show a message about mandatory cookies */
  "mandatoryCta": true /* Show the disabled accept button when mandatory on */
});