var PaymentLastTooltip = "";
var PaymentLastTooltipOption = "";
var PaymentLastTooltiplength = "";
var PaymentStatus = "";
var PaymentOptions = {};
var TranslationLastSelectedElement;

function findGetParameter(parameterName) {
	var result = null,
		tmp = [];
	var items = location.search.substr(1).split("&");
	for (var index = 0; index < items.length; index++) {
		tmp = items[index].split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	}
	return result;
}

function auth_popup(provider) {
	window.location.href = "https://me.ryzom.com/api/auth/external_provider?provider="+provider+"&scope=all&from="+encodeURI(window.location.href);

/*
	if (provider)
		window.open("https://me.ryzom.com/auth/callback?provider="+provider+"&scope=all&from="+encodeURI(window.location.href), "authWindow", "width=800,height=600,scrollbars=yes");
	else
		window.open("https://me.ryzom.com/auth?scope=all&from="+encodeURI(window.location.href), "authWindow", "width=800,height=600,scrollbars=yes");
	return false;*/
}


function open_auth() {
	window.open("https://me.ryzom.com/api/auth?from="+encodeURI(window.location.href), "authWindow", "width=800,height=600,scrollbars=yes");
	return false;
}

function open_ryzom_auth() {
	window.open("https://me.ryzom.com/auth/ryzom?from="+encodeURI(window.location.href), "authWindow", "width=800,height=600,scrollbars=yes");
	return false;
}

function confirm_open_url(text, url) {
	if (confirm(text))
		window.location.href = url;
}
 
function logout() {
	window.location.href = "https://me.ryzom.com/api/auth/logout?from="+encodeURI(window.location.href);
	return false;
}

function closeAndRefreshParent() {
	window.opener.location.reload(true);
	window.close();
	return false;
}

function showPassword(button, input_id) {
	
	var e = document.getElementById(input_id);
	if (e.type === "password") {
		e.type = "text";
		button.classList.add("fa-eye-slash");
		button.classList.remove("fa-eye");
	} else {
		e.type = "password";
		button.classList.add("fa-eye");
		button.classList.remove("fa-eye-slash");
	}
} 


// Expand / Collapse
function toggleVisibility(event)  {
	event.preventDefault();
	var expandedContent = event.target.nextElementSibling;
	if (!expandedContent || !expandedContent.classList.contains("expanded")) {
		console.log(event);
		return;
	}

	if (expandedContent.style.display == "block") {
		expandedContent.classList.remove("animated", "slideInUp");
		expandedContent.classList.add("animated", "slideOutDown");
		expandedContent.style.display = "none";
		event.target.classList.remove("opened");
		event.target.classList.add("closed");
	} else {
		expandedContent.classList.remove("animated", "slideOutDown");
		expandedContent.classList.add("animated", "slideInUp");
		expandedContent.style.display = "block";
		event.target.classList.remove("closed");
		event.target.classList.add("opened");
	}
}

function enableToggleVisibility(class_name) {
	var sections = document.getElementsByClassName(class_name);
	for (var i = 0; i < sections.length; i++) {
		sections[i].addEventListener("click", toggleVisibility);
		sections[i].classList.add("closed");
	}

}

function show(id, state) {
	if (state != undefined & state == false)
		return hide(id);
	var e = document.getElementById(id);
	if (e != undefined)
		e.classList.remove("hidden");
	return false;
}


function hide(id, state) {
	if (state != undefined & state == false)
		return show(id);
	var e = document.getElementById(id);
	if (e != undefined)
		e.classList.add("hidden");
	return false;
}

function switchHidden(id) {
	var e = document.getElementById(id);
	if (e != undefined)
		e.classList.toggle("hidden");
}

function switchDisplay(id) {
	var e = document.getElementById(id);
	if (e != undefined) {
		if (e.style.display == "block")
			e.style.display = "none";
		else
			e.style.display = "block";
	}
}



function addClass(id, name) {
	var e = document.getElementById(id);
	if (e != undefined)
		e.classList.add(name);
}

function removeClass(id, name) {
	var e = document.getElementById(id);
	if (e != undefined)
		e.classList.remove(name);
}

function switchClass(id, name) {
	var e = document.getElementById(id);
	if (e != undefined)
		e.classList.toggle(name);
}

function switchShard(id) {
	document.getElementById("atys-link").classList.remove("selected");
	document.getElementById("yubo-link").classList.remove("selected");
	document.getElementById("gingo-link").classList.remove("selected");
	
	if (id != "atys")
		document.getElementById("atys").classList.add("hidden");
	else
		document.getElementById("atys-link").classList.add("selected");
	
	if (id != "yubo")
		document.getElementById("yubo").classList.add("hidden");
	else
		document.getElementById("yubo-link").classList.add("selected");
	
	if (id != "gingo")
		document.getElementById("gingo").classList.add("hidden");
	else
		document.getElementById("gingo-link").classList.add("selected");
		
	document.getElementById(id).classList.toggle("hidden");
}

function showPaymentTooltip(selection, bg) {
	if (selection == undefined) {
		document.getElementById("payment-tooltip").innerText = PaymentLastTooltip;
		document.getElementById("payment-tooltip-option").innerText = PaymentLastTooltipOption;
		document.getElementById("payment-tooltip-length").innerText = PaymentLastTooltiplength;
	} else {
		PaymentLastTooltip = document.getElementById("payment-tooltip").innerText;
		PaymentLastTooltipOption = document.getElementById("payment-tooltip-option").innerText
		PaymentLastTooltiplength = document.getElementById("payment-tooltip-length").innerText
		document.getElementById("payment-tooltip-option").innerText = ""
		document.getElementById("payment-tooltip-length").innerText = ""
		document.getElementById("payment-tooltip").innerText = document.getElementById(selection+"-tooltip").innerText;
	}
}

function selectPaymentOption(selection) {
	document.getElementById("providers").classList.remove("hidden");

	if (PaymentStatus == "waiting")
		return;

	var childs = document.getElementById("providers").children;
	for (i = 0; i < childs.length; i++) {
		childs[i].classList.add("hidden");
		document.getElementById(childs[i].id+"-link").classList.remove("selected");
	}

	document.getElementById(selection).classList.remove("hidden");
	document.getElementById(selection+"-link").classList.add("selected");
	PaymentLastTooltip = "";
	PaymentLastTooltipOption = document.getElementById(selection+"-link").getElementsByTagName("strong")[0].innerText;
	document.getElementById("payment-tooltip-length").innerText = "";
	PaymentLastTooltiplength = "";
}

function selectPayment(selection, provider) {
	var childs = document.getElementById(provider+"-prices").children;
	for (i = 0; i < childs.length; i++) {
		childs[i].classList.remove("selected");
	}
	
	PaymentOptions["length"] = selection
	PaymentOptions["provider"] = provider

	show("payment");

	if (PaymentStatus != "waiting") {
		show("payment-waiting")
		hide("payment-content");
		document.getElementById(provider+"-"+selection+"-link").classList.add("selected");
		PaymentLastTooltiplength = " "+document.getElementById(provider+"-"+selection+"-link").getElementsByTagName("strong")[0].innerText;
		tooltip = PaymentLastTooltiplength.replace("\n", " ").split(" ")
		if (tooltip[3])
			document.getElementById("payment-tooltip-length").innerText = " "+tooltip[1]+" "+tooltip[2]+" ("+tooltip[3]+")";
		else
			document.getElementById("payment-tooltip-length").innerText = " "+tooltip[1]+" "+tooltip[2];
		console.log(document.getElementById("payment-tooltip-length").innerText);
		var urlAjaxForm = "/billing/payment?"+window.location.search.substr(1)+"&provider="+provider+"&length="+selection;
		PaymentStatus = "waiting"
		console.log(urlAjaxForm);
		
		
		var httpRequest = new XMLHttpRequest();
		httpRequest.onreadystatechange = function(data) {
			if (this.readyState == 4 && this.status == 200) {
				PaymentStatus = "Ready"
				var json = this.responseText;
				console.log(json);
				var values = JSON.parse(json);
				if (values["html"] != undefined)
					document.getElementById("payment-content").innerHTML = values["html"];
				else
					document.getElementById("payment-content").innerHTML = document.getElementById("payment-content-error").innerHTML;
				if (values["braintree_token"] != undefined)
					setupBrainTree(selection, provider, values["locale"], values["braintree_token"]);
				else if (values["xsolla_token"] != undefined) 
					startXsollaPayment(values["xsolla_token"], values["xsolla_sandbox"]);
				hide("payment-waiting");
				show("payment-content");
			}
		};
		httpRequest.open("GET", urlAjaxForm);
		httpRequest.send();
	}
}

function startXsollaPayment(token, sandbox)
{
	if (sandbox)
		console.log("sandbox")
	else
		console.log("no sandbox")
	var options = {
		access_token: token,
		sandbox: sandbox
	};
	
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.async = true;
	s.src = "//static.xsolla.com/embed/paystation/1.0.7/widget.min.js";
	s.addEventListener("load", function (e) {
		console.log("loaded");
		XPayStationWidget.init(options);
	}, false);
	var head = document.getElementsByTagName("head")[0];
	head.appendChild(s);
}

function startPayment(selection, provider) {
	if (PaymentStatus != "waiting") {
		show("payment-waiting")
		hide("payment-content");

		var urlAjaxForm = "/billing/payment?provider="+provider+"&length="+selection+"&nonce=1";
		console.log(urlAjaxForm)
		PaymentStatus = "waiting";
		
		var httpRequest = new XMLHttpRequest();
		httpRequest.onreadystatechange = function(data) {
			if (this.readyState == 4 && this.status == 200) {
				PaymentStatus = "Ready";
				var json = this.responseText;
				var values = JSON.parse(json);
				console.log(values);
				document.getElementById("payment-content").innerHTML = values["html"];
				if (values["braintree_token"] != undefined) {
					hide("payment-waiting");
					show("payment-content");
					setupBrainTree(selection, provider, values["locale"], values["braintree_token"]);
				} else if (values["submit_form"] != undefined) {
					show("payment-waiting")
					hide("payment-content");
					submitForm(selection, provider);
				}
			}
		};
		httpRequest.open("GET", urlAjaxForm);
		httpRequest.send();
	}
}

function runAjax(url, id, no_loading) {
	var e = null;
	if (id)
		e = document.getElementById(id);
	var httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function(data) {
		if (this.readyState == 4 && this.status == 200) {
			if (e)
				e.innerHTML = this.responseText;
		}
	};
	if (e && no_loading != true )
		e.innerHTML = "<img class='round' src='/images/waiting.gif' />";
	httpRequest.open("GET", url);
	httpRequest.send();
	return false;
}

function postAjax(url, id, data, waiting) {
	var e = null;
	if (id)
		e = document.getElementById(id);
	
	var httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function(data) {
		if (this.readyState == 4 && this.status == 200) {
			console.log( this.responseText);
			if (e)
				e.innerHTML = this.responseText;
		}
	};
	
	httpRequest.open("POST", url);
	httpRequest.send(data);
	
	if (e) {
		if (waiting == undefined)
			e.innerHTML = "<img class='round' src='/images/waiting.gif' />";
		else if (waiting != "")
			e.innerHTML = waiting;
	}
}

function postFormAjax(url, id, formId, waiting) {
	var e = null;
	if (id)
		e = document.getElementById(id);

	var httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function(data) {
		if (this.readyState == 4 && this.status == 200) {
			e.innerHTML = this.responseText;
		}
	};
	httpRequest.open("POST", url);
	var data = new FormData();
	form = document.getElementById(formId)
	for ( var i = 0; i < form.elements.length; i++ ) {
		var form_e = form.elements[i];
		console.log(form_e);
		console.log(form_e.type);
		if (form_e.type == "checkbox") {
			if (form_e.checked)
				data.append(form_e.name.substr(0, form_e.name.length-1)+form_e.value+"]", 1);
		} else
			data.append(form_e.name, form_e.value);
		//form_e.value = "";
	}

	httpRequest.send(data);

	if (e) {
		if (waiting == undefined)
			e.innerHTML = "<img class='round' src='/images/waiting.gif' />";
		else if (waiting != "")
			e.innerHTML = waiting;
	}
	return false;
}


function ajaxCheck(id, url) {
	var e = document.getElementById("check-"+id)
	console.log(id)
	console.log(e)
	if (e.classList.contains("padding-check"))
		url += "&command=unvalidate"
	else
		url += "&command=validate"
		
	var httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function(data) {
		if (this.readyState == 4 && this.status == 200) {
			if (e.classList.contains("padding-check")) {
				e.classList.remove("fas", "fa-check-square", "badge-secondary", "padding-check");
				e.classList.add("far", "fa-square");
			} else {
				e.classList.remove("far", "fa-square");
				e.classList.add("fas", "fa-check-square", "badge-secondary", "padding-check");
			}
		}
	};
	httpRequest.open("GET", url);
	httpRequest.send();
	return false;
}


function setupBrainTree(selection, provider, locale, client_token) {
	var form = document.querySelector("#payment-form");
	var button = document.querySelector("#payment-submit");

	braintree.dropin.create({
		authorization: client_token,
		container: "#payment-container",
		googlePay: {
			googlePayVersion: 2,
			merchantId: "BCR2DN6T3PROFTYU",
			transactionInfo: {
				totalPriceStatus: "FINAL",
				totalPrice: "1.00",
				currencyCode: "EUR",
			}
		},
	
		threeDSecure: true,
		dataCollector: {},
		locale: "fr",
		currency: "EUR",
		enableBillingAddress: true,
		card: { cardholderName: { required: true} },
		},
	function (err, dropinInstance) {
		console.log("Request Payment Method Error", err);
		
		button.addEventListener("click", function () {
			console.log(dropinInstance.requestPaymentMethod);
			dropinInstance.requestPaymentMethod({
				threeDSecure: {
				  amount: "1.00",
				  email: "ulukyn@gmail.com",
				 /* billingAddress: {
					givenName: billingFields['billing-given-name'].input.value,
					surname: billingFields['billing-surname'].input.value,
					phoneNumber: billingFields['billing-phone'].input.value.replace(/[\(\)\s\-]/g, ''), // remove (), spaces, and - from phone number
					streetAddress: billingFields['billing-street-address'].input.value,
					extendedAddress: billingFields['billing-extended-address'].input.value,
					locality: billingFields['billing-locality'].input.value,
					region: billingFields['billing-region'].input.value,
					postalCode: billingFields['billing-postal-code'].input.value,
					countryCodeAlpha2: billingFields['billing-country-code'].input.value
				  }*/
				}
			},
			function (err, payload) {
				if (err) {
					console.log("Request Payment Method Error", err);
					return;
				}
				// Add the nonce to the form and submit
				document.querySelector("#nonce").value = payload.nonce;
				document.querySelector("#device_data").value = payload.deviceData;
				document.querySelector("#length").value = selection;
				document.querySelector("#provider").value = provider;
				show("payment-waiting")
				hide("payment-content");
				form.submit();
			});
		});
	});
}

function submitForm(selection, provider) {
	var form = document.querySelector("#payment-form");

	document.querySelector("#nonce").value = Math.random().toString(36).substr(2, 9);
	document.querySelector("#length").value = selection;
	document.querySelector("#provider").value = provider;
	form.submit();
}


function openUrl(url) {
	surl = url.split(":");
	location.href = window.location.origin+"/"+surl.join("/");
}

function openWindow(url) {
	surl = url.split(":");
	window.open(window.location.origin+"/"+surl.join("/"));
}

function openPopup(url, name, width, height) {
	window.open(url, name,"menubar=no, status=no, scrollbars=no, menubar=no, width="+width+", height="+height);
}


function submitAndDisable(element) {
	element.disabled="disabled";
	element.classList.add("disabled");
	setTimeout(function () {
		element.disabled="";
	element.classList.remove("disabled");
	}, 1000);
}


function updateNewTicketMessage(element) {
	hide("jforms_support_ticket_technical_optional_crash");
	hide("jforms_support_ticket_technical_optional_crash_label");
	hide("jforms_support_ticket_technical_optional_mission_name");
	hide("jforms_support_ticket_technical_optional_mission_name_label");
	hide("jforms_support_ticket_technical_optional_mission_giver");
	hide("jforms_support_ticket_technical_optional_mission_giver_label");
	if (element == "missions") {
		show("jforms_support_ticket_technical_optional_mission_name");
		show("jforms_support_ticket_technical_optional_mission_giver");
		show("jforms_support_ticket_technical_optional_mission_name_label");
		show("jforms_support_ticket_technical_optional_mission_giver_label");
	} else if (element == "crash") {
		show("jforms_support_ticket_technical_optional_crash");
		show("jforms_support_ticket_technical_optional_crash_label");
	}
}


function newTicketOncharChange(section, element) {
	console.log(element)
	show("jforms_support_ticket_"+section+"_for_char", element == "_another")

}
