sendTranslations = undefined;

function showRArrowInTextareaLine(id, line) {
	var element = document.getElementById(id);
	var lines = element.value.split("⇶ ").join("").split("\n");
	if (lines[line] == undefined)
		lines[line] = "⇶ ";
	else
		lines[line] = "⇶ "+lines[line];
	element.value = lines.join("\n")
}

function hideRArrowInTextareaLine(id, line) {
	var element = document.getElementById(id);
	var lines = element.value.split("⇶ ").join("").split("\n");
	element.value = lines.join("\n")
}



function saveTranslations() {
	var record_id = TranslationLastSelectedElement.id.substr(9);
	var url = document.getElementById("translations").action+"&ajax=1&record_id="+record_id;
	postFormAjax(url, "tr-"+record_id, "translations", "");
}

function createTranslationOptions(id, options, element) {
	var select_options = {"faction" : "Faction", "race" : "Race", 
		"fameFy" : "Fame Fyros", "fameMa" : "Fame Matis", "fameTr" : "Fame Tryker", "fameZo" : "Fame Zorai", "fameKm" : "Fame Kami", "fameKr" : "Fame Kara", "fameMr" : "Fame Marauder",
		};
	var compare_options = {"equal":"=", "gt":">", "ge":"&ge;", "lt":"<", "le":"&le;", "diff":"&ne;"};
	var bool_options = {"": "", "&":"AND", "|":"OR"};
	var html = "<table>";
	var elements = options.trim().split(" ");
	var id_option = 0;
	console.log(elements);
	offset = 3;
	for(var i = 0; i < 12; i += offset) {
		html += "<tr>";
		html += "<td>";
		var x = i;
		if (x != 0) {
			console.log(x, elements[x]);
			var option = ""
			if (elements[x] != undefined)
				option = elements[x];
			html += "<select id='tr-bool-"+id+"-"+id_option+"'>";
			for(var k in bool_options) {
				if (option == k)
					html += "<option value='"+k+"' selected='selected'>"+bool_options[k]+"</option>";
				else
					html += "<option value='"+k+"'>"+bool_options[k]+"</option>";
			}
			html += "</select>";
			x++;
			offset = 4;
		}
		html += "</td>";
		
		html += "<td>";
		var option = ""
		if (elements[x] != undefined)
			option = elements[x];
		html += "<select id='tr-select-"+id+"-"+id_option+"'>";
		html += "<option value=''></option>";
		for(var k in select_options) {
			if (option == k)
				html += "<option value='"+k+"' selected='selected'>"+select_options[k]+"</option>";
			else
				html += "<option value='"+k+"'>"+select_options[k]+"</option>";
		}
		html += "</select>";
		html += "</td>";
		x++;
		
		var compare = ""
		if (elements[x] != undefined)
			compare = elements[x];
		html += "<td>";
		html += "<select id='tr-compare-"+id+"-"+id_option+"'>";
		for(var k in compare_options) {
			if (compare == k)
				html += "<option value='"+k+"' selected='selected'>"+compare_options[k]+"</option>";
			else
				html += "<option value='"+k+"'>"+compare_options[k]+"</option>";
		}
		html += "</select>";
		html += "</td>";
		x++;
		
		var value = ""
		if (elements[x] != undefined)
			value = elements[x];
		html += "<td>";
		html += "<input style='width: 100px' id='tr-value-"+id+"-"+id_option+"' value='"+value+"'/>";
		html += "</td>";
		
		html += "</tr>";
		id_option++;
	}
	html += "</table>";
	return html;
}

function addTranslationOptions(id) {
	var element = document.getElementById(id);
	element.value += "\n"+"// ";
	TranslationLastSelectedElement = "";
	//displayTranslationArkOptions(element);
	return false;
}
	
function displayTranslationArkOptions(element) {
	if (TranslationLastSelectedElement)
		hideTranslationOptions(TranslationLastSelectedElement);
	TranslationLastSelectedElement = element;
	var text = element.value;
	var stext = text.split("//");
	var parts = [];
	var tr_id = document.getElementById(element.id.substring(6));
	var tr_input = document.getElementById("input-src-"+element.id.substring(6));
	if (stext.length > 1) { 
		element.readOnly = true;
		html = "";
		for (var i = 0; i < stext.length; i++) {
			if (stext[i]) {
				lines = stext[i].split("\n");
				html += "<tr align='center' class='tr-options'><td>#"+i+"</td> <td>"+createTranslationOptions(i, lines.shift(), element)+"</td> <td></td> <td><textarea class='expand' id='tr-textarea-"+i+"'>"+lines.join("\n")+"</textarea></td><td></td><td></td></tr>";
			}
		}
		html += "<tr class='tr-options last'></tr>";
		html += "<tr><td><a href='#' onclick='return addTranslationOptions(\'"+element.id+"; return false;\')'>ADD</button></td></tr>";
		tr_id.insertAdjacentHTML("afterend", html);
	} else {
		tr_id.classList.add("border-bottom-blue");
		element.classList.add("expand");
	}
	tr_id.classList.add("selected");
	tr_input.classList.add("expand");

	console.log(parts);
}



function displayTranslationWordsOptions(element, src_lang, lang, header) {
	if (element == TranslationLastSelectedElement)
		return;

	var headers = header.split("\t")
	var headerPos = {}
	for (var i = 0; i < headers.length; i++)
		headerPos[headers[i].trim()] = i-2;
	
	var optionsType = {
		"creature ID": {
			"wk": [﻿"ia", "da", "name", "pia", "pda", "p"],
			"de": ["iaa", "ia", "iag", "da", "dag", "daa", "dad", "pda", "﻿name", "named", "namedda", "nameda", "pia", "pd", "p"],
			"en": [﻿"ia", "da", "name", "pia", "pda", "p"],
			"es": [﻿"ia", "da", "name", "pia", "pda", "p"],
			"fr": ["ia", "da", "cc", "pr", "name", "pia", "pda", "p"],
			"ru": ["﻿name", "p", "creagen", "screagen"],
		},
		"item ID": {
			"wk": [﻿"ia", "da", "name", "pia", "pda", "p", "description"],
			"de": ["ia", "da", "pda", "﻿name", "named", "nameda", "pia", "pd", "p", "description"],
			"en": [﻿"ia", "da", "name", "pia", "pda", "p", "description"],
			"es": [﻿"ia", "da", "name", "pia", "pda", "p", "description"],
			"fr": [﻿"ia", "da", "name", "pia", "pda", "p", "description"],
			"ru": ["﻿name", "p", "description", "onegen",  "twogen", "somgen"],
		},
		"sbrick ID": {
			"wk": [﻿"ia", "da", "name", "pia", "pda", "p", "description", "description2"],
			"de": ["ia", "da", "pda", "﻿name", "named", "pia", "pd", "p", "description", "description2"],
			"en": [﻿"ia", "da", "name", "pia", "pda", "p", "description", "description2"],
			"es": [﻿"ia", "da", "name", "pia", "pda", "p", "description", "description2"],
			"fr": [﻿"ia", "da", "name", "pia", "pda", "p", "description", "description2"],
			"ru": ["﻿name", "description", "description2"],
		},
		"placeId": {
			"wk": [﻿"da", "name"],
			"de": ["da", ﻿"name", "daa", "namea", "pp", "nameda", "lpp"],
			"en": [﻿"da", "name"],
			"es": [﻿"da", "name"],
			"fr": ["da", ﻿"pr", "name", "pda", "ppr", "p",],
			"ru": ["name", "placegen", "placeaccus", "placedat"],
		},
		"title_id": {
			"wk": [﻿"name", "women_name"],
			"de": [﻿"name", "women_name"],
			"en": [﻿"name", "women_name"],
			"es": [﻿"name", "women_name"],
			"fr": [﻿"name", "women_name"],
			"ru": [﻿"name", "women_name"],
		},
		"bot name": {
			"wk": [﻿"translated name", "sheet_name"],
			"de": [﻿"translated name", "sheet_name"],
			"en": [﻿"translated name", "sheet_name"],
			"es": [﻿"translated name", "sheet_name"],
			"fr": [﻿"translated name", "sheet_name"],
			"ru": [﻿"translated name", "sheet_name"],
		},
	}
	
	var optionsValues = {
		"wk":
			{
				"ia": ["<c>Singular Article, Indefinite:</> <o>An</> object / <o>A</> weapon","","a","an"],
				"da": ["<c>Singular Article, Definite:</> <o>The</> object","","the"],
				"pia": ["<c>Plural Article, Indefinite:</> <o>(empty)</> Objects",""],
				"pda": ["<c>Plural Article, Definite:</> <o>The</> objects","","the"],
				"name": ["<c>Singular</>"],
				"women_name": ["<c>Female Name</>"],
				"p": ["<c>Pural</>"],
				"pr": ["???"],
				"ppr": ["???"],
				"sheet_name": ["<c>NOT NEED</>"],
				"description": ["<c>Description</>"],
				"description2": ["<c>Description2</>"],
				"translated name": ["<c>Name <o>use $title$ to translate a npc/object name</></>"],
			},
		"en":
			{
				"ia": ["<c>Singular Article, Indefinite:</> <o>An</> object / <o>A</> weapon","","a","an"],
				"da": ["<c>Singular Article, Definite:</> <o>The</> object","","the"],
				"pia": ["<c>Plural Article, Indefinite:</> <o>(empty)</> Objects",""],
				"pda": ["<c>Plural Article, Definite:</> <o>The</> objects","","the"],
				"name": ["<c>Singular</>"],
				"women_name": ["<c>Female Name</>"],
				"p": ["<c>Pural</>"],
				"sheet_name": ["<c>NOT NEED</>"],
				"description": ["<c>Description</>"],
				"description2": ["<c>Description2</>"],
				"translated name": ["<c>Name <o>use $title$ to translate a npc/object name</></>"],
			},
		"de":
			{
				"ia": ["<c>Singular Article, Indefinite:</> <o>Eine</> große lebende Skulptur (f) / <o>Ein</> alter Baum (m) / <o>Ein</> schmales Bett (n)", "", "eine", "ein"],
				"da": ["<c>Singular Article, Definite:</> <o>Die</> große lebende Skulptur (f) / <o>Der</> alte Baum (m) / <o>Das</> schmale Bett (n)", "", "die", "der", "das"],
				"name": ["<c>Singular</>"],
				"women_name": ["<c>Female Name</>"],
				"iaa": ["<c>Singular, With its nominative definite article:</> (Eine/Die) <o>große lebende Skulptur</> / (Der) <o>alte Baum</> / (Das) <o>schmale Bett</>", "", "eine", "die", "der", "das"],
				"iag": ["<c>Singular, Accusative:</> Die <o>große lebende Skulptur</> / Den <o>alten Baum</> / Das <o>schmale Bett</>", "", "die", "den", "das"],
				"dag": ["???", "", "des"],
				"daa": ["???", "", "den"],
				"dad": ["???", "", "dem"],
				"pia": ["<c>Plural Article, Definite:</> <o>Einen</> große lebende Skulptur","","einen"],
				"pd": ["<c>Plural dative</>"],
				"pda": ["<c>Plural Article, Definite:</> <o>Die</> große lebende Skulptur", "", "die"],
				"p": ["<c>Plural</>"],
				"pp": ["???"],
				"lpp": ["???"],
				"namedda": ["???"],
				"nameda": ["???"],
				"named": ["???"],
				"sheet_name": ["<c>NOT NEED</>"],
				"description": ["<c>Description</>"],
				"description2": ["<c>Description2</>"],
				"translated name": ["<c>Name <o>use $title$ to translate a npc/object name</></>"],
			},
		"fr":
			{
				"ia": ["<c>Article Indefini, Singulier : </> <o>un</> objet /  <o>une</> arme", "","un","une"], 
				"da": ["<c>Article Défini, Singulier : </> <o>l'</>objet / <o>le</> chemin / <o>l'</>arme / <o>la</> maison", "","l'","la","le"],
				"pia": ["<c>Article Indefini, Pluriel : </> <o>des</> objets", "","des"], 
				"pda": ["<c>Article Défini, Pluriel : </> <o>les</> objets", "","les"], 
				"name": ["<c>Singulier</> "],
				"women_name": ["<c>Female Name</>"],
				"cc": ["<c>CC : </> <o>du</> pistil / <o>de la</> viande", "","du", "de la", "de l'"],
				"pr": ["<c>PR : </> <o>au</> pistil / <o>à la</> viande / <o>dans le</> lac", "","au", "à l'", "à la", "dans le", "dans la", "dans l'"],
				"ppr": ["<c>PPR : </> <o>aux</> pistils / <o>dans les</> lacs", "", "à", "aux", "dans les"],
				"p": ["<c>Puriel</> "],
				"sheet_name": ["<c>NOT NEED</>"],
				"description": ["<c>Description</>"],
				"description2": ["<c>Description2</>"],
				"translated name": ["<c>Name <o>use $title$ to translate a npc/object name</></>"],
				},
		"es":
			{
				"ia": ["<c>Singular Article, Indefinite:</> <o>Please send to Ulukyn some examples</>", "","uno","una"],
				"da": ["<c>Singular Article, Definite:</> <o>Please send to Ulukyn some examples</>", "","el", "la"],
				"pia": ["<c>Plural Article, Indefinite:</> <o>Please send to Ulukyn some examples</>", "","unos", "unas"],
				"pda": ["<c>Plural Article, Definite:</> <o>Please send to Ulukyn some examples</>", "","los", "las"],
				"name": ["<c>Singular</>"],
				"women_name": ["<c>Female Name</>"],
				"p": ["<c>Pural</> "],
				"sheet_name": ["<c>NOT NEED</>"],
				"description": ["<c>Description</>"],
				"description2": ["<c>Description2</>"],
				"translated name": ["<c>Name <o>use $title$ to translate a npc/object name</></>"],
			},
		"ru":
			{
				"name": ["<c>Singular</>"],
				"women_name": ["<c>Female Name</>"],
				"p": ["<c>Plural</>"],
				"creagen": ["<c>One element:</> <o>Please send to Ulukyn some examples</>"],
				"screagen": ["<c>2, 3 or 4 elements:</> <o>Please send to Ulukyn some examples</>"],
				"somgen": ["5+ elements"],
				"twogen": ["2-4 elements"],
				"onegen": ["1 element"],
				"placegen": ["1 place ?"],
				"placeaccus": ["<c>NOT USED</>"],
				"placedat" : ["<c>NOT USED</>"],
				"sheet_name": ["<c>NOT NEED</>"],
				"description": ["<c>Description</>"],
				"description2": ["<c>Description2</>"],
				"translated name": ["<c>Name <o>use $title$ to translate a npc/object name</></>"],
				//"sceagen": ["<c>5 elements or more:</> <o>Please send to Ulukyn some examples</>"],
			},
	}
	
	if (TranslationLastSelectedElement)
		sendTranslations();
	TranslationLastSelectedElement = element;
	var text = element.value;
	var stext = text.split("⇶ ").join("").split("\n");
	console.log(stext);
	var parts = [];
	var tr_id = document.getElementById(element.id.substring(6));
	var src_tr_id = document.getElementById("input-src-"+element.id.substring(6));
	element.readOnly = true;
	var html = "";
	var options = "";
	var tooltips = "<div style='height: 24px'>";
	console.log(headers[1]);
	for (var x = 0; x < optionsType[headers[1]][lang].length; x++) {
		src_header = optionsType[headers[1]][src_lang][x];
		header = optionsType[headers[1]][lang][x].trim();
		i = headerPos[header];
		if (stext[i] == undefined)
			stext[i] = "";
		console.log("["+header+"]", headerPos, i)
		console.log(headerPos, optionsValues[lang], optionsValues[lang][header])
		var tooltip = ""
		if (optionsValues[lang][header] != undefined)
			tooltip = "["+header+"]"+optionsValues[lang][header][0];
		
		tooltip = tooltip.split("<c>").join("<span class='cyan'>").split("<o>").join("<span class='orange'>").split("</>").join("</span>");
		tooltips += "<div class='hidden' id='words-tooltip-"+i+"'>"+tooltip+"</div>";
//		if (tooltip) {
			var src_id = "input-src"+element.id.substr(5);
			var src_i = headerPos[src_header];
			var option_list = optionsValues[lang][header];
			if (option_list == undefined)
				option_list = [""]
			if (option_list.length > 1) {
				html += "<td><select onmouseover=\"showRArrowInTextareaLine('"+src_id+"', "+src_i+"); showRArrowInTextareaLine('"+element.id+"', "+i+"); show('words-tooltip-"+i+"')\" onmouseleave=\"hideRArrowInTextareaLine('"+src_id+"', "+src_i+"); hideRArrowInTextareaLine('"+element.id+"', "+i+"); hide('words-tooltip-"+i+"')\" id=\"words-option-"+i+"\">";
				if (optionsValues[lang][header].indexOf(stext[i]) == -1)
					html += "<option name='"+stext[i]+"' selected='selected'>"+stext[i]+"</option>";
				for (var j = 1; j < option_list.length; j++) {
					if (option_list[j] == undefined)
						option_list[j] = "";
					if (j == 2)
						html += "<option name='"+option_list[j]+"' selected='selected'>"+option_list[j]+"</option>";
					else
						html += "<option name='"+option_list[j]+"'>"+option_list[j]+"</option>";
				}
				html += "</select></td>";
			} else {
				html += "<td><textarea id='words-option-"+i+"' onmouseover=\"showRArrowInTextareaLine('"+src_id+"', "+src_i+"); showRArrowInTextareaLine('"+element.id+"', "+i+"); show('words-tooltip-"+i+"')\" onmouseleave=\"hideRArrowInTextareaLine('"+src_id+"', "+src_i+"); hideRArrowInTextareaLine('"+element.id+"', "+i+"); hide('words-tooltip-"+i+"')\" >"+stext[i]+"</textarea></td>";
			}
			
		if (x != 0 && x % 7 == 0)
			html += "</tr><tr>";
//			} else {
//				html += "<td><textarea id='words-option-"+i+"' onmouseover=\"showRArrowInTextareaLine('"+src_id+"', "+src_i+"); showRArrowInTextareaLine('"+element.id+"', "+i+"); show('words-tooltip-"+i+"')\" onmouseleave=\"hideRArrowInTextareaLine('"+src_id+"', "+src_i+"); hideRArrowInTextareaLine('"+element.id+"', "+i+"); hide('words-tooltip-"+i+"')\" >"+stext[i]+"</textarea></td>";
//			}
//		} else
//			html += "<input type='hidden' id='words-option-"+i+"'/>";
	}
	tooltips += "</div>";
	html = "<tr align='center' class='tr-options' height='60px'><td colspan='6'>"+tooltips+"<table cellpadding='0' cellspacing='0' width='100%'><tr>"+html+"</tr></table></td></tr><tr align='center' class='tr-options'><td colspan='6' height='40px'><button type='button' onclick='sendTranslationWordsOptions()' class='badge-primary round' name='save'>Save (ctrl+s)</button></td></tr><tr class='tr-options last'></tr>";
	tr_id.insertAdjacentHTML("afterend", html);
	element.classList.add("expand");
	src_tr_id.classList.add("expand");
	
	tr_id.classList.add("selected");
	console.log(parts);
}

function sendTranslationWordsOptions() {
	var values = [];
	var i = 0;
	var e = document.getElementById("words-option-"+i);
	while (e != undefined) {
		values.push(e.value);
		console.log("["+e.value+"]");
		i++;
		var e = document.getElementById("words-option-"+i);
	}

	TranslationLastSelectedElement.value = values.join("\n");
	hideTranslationOptions(TranslationLastSelectedElement);
	saveTranslations();
}

function displayTranslationUxtOptions(element) {
	if (TranslationLastSelectedElement != element) {
		if (TranslationLastSelectedElement)
			sendTranslations();
		TranslationLastSelectedElement = element;
		var tr_id = document.getElementById(element.id.substring(6));
		tr_id.classList.add("border-bottom-blue");
		tr_id.classList.add("selected");
		element.classList.add("expand");
	}
}

function displayTranslationWebMeOptions(element) {
	element.classList.add("expand");
	if (TranslationLastSelectedElement != element) {
		if (TranslationLastSelectedElement)
			sendTranslations();
		TranslationLastSelectedElement = element;
		var tr_id = document.getElementById(element.id.substring(6));
		tr_id.classList.add("border-bottom-blue");
		tr_id.classList.add("selected");
	}
}


function hideTranslationOptions(element) {
	var tr_id = document.getElementById(element.id.substring(6));
	var src_tr_id = document.getElementById("input-src-"+element.id.substring(6));
	src_tr_id.classList.remove("expand");
	tr_id.classList.remove("selected");
	element.classList.remove("expand");
	if (element.readOnly == true) {
		element.readOnly = false;
		var paras = document.getElementsByClassName("tr-options");
		while(paras[0])
			paras[0].parentNode.removeChild(paras[0]);
	} else {
		tr_id.classList.remove("border-bottom-blue");
	}
}