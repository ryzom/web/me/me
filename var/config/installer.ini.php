;<?php die(''); ?>
; for security reasons , don't remove or modify the first line
; don't modify this file if you don't know what you do. it is generated automatically by jInstaller


[modules]
jelix.installed=1
jelix.version=1.7.0
jelix.version.date="2019-09-10 00:29"
jelix.firstversion=1.7.0
jelix.firstversion.date="2019-09-10 00:29"

me.installed=1
me.version=0.1pre
me.version.date=2019-10-10
me.firstversion=0.1pre
me.firstversion.date=2019-10-10

jfeeds.installed=0
jfeeds.version=
jacldb.installed=0
jacldb.version=
jacl2.installed=0
jacl2.version=
jauth.installed=1
jauth.version=1.7.0
jacl.installed=0
jacl.version=
jacl2db.installed=0
jacl2db.version=
jauthdb.installed=0
jauthdb.version=
jpref.installed=0
jpref.version=
master_admin.installed=0
master_admin.version=
jauthdb_admin.installed=0
jauthdb_admin.version=
jpref_admin.installed=0
jpref_admin.version=
jacl2db_admin.installed=0
jacl2db_admin.version=
jauth.version.date="2019-09-10 00:29"
jauth.firstversion=1.7.0
jauth.firstversion.date="2019-09-10 00:29"

billing.installed=1
billing.version=0.1pre
billing.version.date=2019-10-10
billing.firstversion=0.1pre
billing.firstversion.date=2019-10-10

utils.installed=1
utils.version=0.1pre
utils.version.date=2019-12-11
utils.firstversion=0.1pre
utils.firstversion.date=2019-12-11

ryzom.installed=1
ryzom.version=0.1pre
ryzom.version.date=2019-12-15
ryzom.firstversion=0.1pre
ryzom.firstversion.date=2019-12-15

api.installed=1
api.version=0.1pre
api.version.date=2019-12-15
api.firstversion=0.1pre
api.firstversion.date=2019-12-15

translator.installed=1
translator.version=0.1pre
translator.version.date=2020-04-01
translator.firstversion=0.1pre
translator.firstversion.date=2020-04-01

support.installed=1
support.version=0.1pre
support.version.date=2020-08-22
support.firstversion=0.1pre
support.firstversion.date=2020-08-22

marketing.installed=1
marketing.version=0.1pre
marketing.version.date=2020-08-22
marketing.firstversion=0.1pre
marketing.firstversion.date=2020-08-22

admin.installed=1
admin.version=0.1pre
admin.version.date=2022-02-18
admin.firstversion=0.1pre
admin.firstversion.date=2022-02-18
